/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;



/**
 * A scanner dfa state, it has an edge collection that links it to
 * other states.
 * 
 * @author Alessio Scalici
 */
class BaseScannerState
{
	// the symbol accepted by this state. If this is not a final state, is null
	private TerminalSymbol acceptedSymbol;
	
	// the outgoing edges
	private Edge[] edges;
	
	// a unique hash code
	private int hash;
	
	// an instance counter
	private static int instanceNum = Integer.MIN_VALUE;
	
	{
		hash = instanceNum++; // sets the unique hash
	}
	
	/**
	 * Builds a final state that accepts the specified symbol. If the argument is null,
	 * this will be a non/final state
	 * @param acceptedSymbol
	 */
	BaseScannerState(TerminalSymbol acceptedSymbol)
	{
		super();
		this.acceptedSymbol = acceptedSymbol;
	}
	
	/**
	 * Builds a non/final state
	 */
	BaseScannerState()
	{
		super();
		this.acceptedSymbol = null;
	}

	/**
	 * Returns the symbol accepted by this state, or null if this is not a final state
	 * @return the symbol accepted by this state, or null if this is not a final state
	 */
	TerminalSymbol getAcceptedSymbol()
	{
		return acceptedSymbol;
	}


	/**
	 * Sets the symbol accepted by this state. This makes this a final state.
	 * @param acceptedSymbol the terminal symbol to accept
	 * @throws NullPointerException if the symbol is null
	 */
	void setAcceptedSymbol(TerminalSymbol acceptedSymbol) throws NullPointerException
	{
		if (acceptedSymbol == null)
			throw new NullPointerException();
		this.acceptedSymbol = acceptedSymbol;
	}

	/**
	 * Returns true if this is a final state, false otherwise
	 * @return true if this is a final state, false otherwise
	 */
	boolean isFinal()
	{
		return (acceptedSymbol != null);
	}
	
	/**
	 * Returns the outgoing edges
	 * @return the outgoing edges
	 */
	Edge[] getEdges()
	{
		return edges;
	}
	
	/**
	 * Sets the outgoing edges
	 * @param edges the edges to set
	 */
	void setEdges(Edge[] eds)
	{
		this.edges = eds;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object arg0)
	{
		if (arg0 == this)
			return true;
		if (arg0 == null || !(arg0 instanceof BaseScannerState))
			return false;
		
		return (hash == arg0.hashCode());
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return hash;
	}
	
	
	@Override
	public void finalize() throws Throwable
	{
		try
		{
			if (edges != null)
				for (int i=0; i<edges.length; ++i)
					edges[i] = null;
		}
		catch (Throwable t)
		{
			throw t;
		}
		finally
		{
			super.finalize();
		}
		
	}

	
}
