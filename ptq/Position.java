/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;

/**
 * A terminal node position. Provides informations about line, column, position of the first character
 * of the token
 * 
 * @author Alessio Scalici
 */
public class Position
{
	// the line
	private int line;
	
	// the column
	private int col;
	
	// the offset index
	private int index;
	
	/**
	 * Builds the default position
	 */
	Position()
	{
		line = 0;
		col = 0;
		index = 0;
	}
	
	/**
	 * Builds a position with the given line, column and position
	 * @param line the row
	 * @param col the column
	 * @param index the offset index
	 */
	Position(int line, int col, int index)
	{
		super();
		this.line = line;
		this.col = col;
		this.index = index;
	}

	/**
	 * @return the line
	 */
	public int getLine()
	{
		return line;
	}
	
	/**
	 * @return the column
	 */
	public int getCol()
	{
		return col;
	}
	
	/**
	 * @return the offset index
	 */
	public int getIndex()
	{
		return index;
	}
	
	
	/**
	 * @param line the line to set
	 */
	void setLine(int line)
	{
		this.line = line;
	}
	
	/**
	 * @param index the index to set
	 */
	void setIndex(int index)
	{
		this.index = index;
	}
	
	/**
	 * @param col the column to set
	 */
	void setCol(int col)
	{
		this.col = col;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object arg0)
	{
		if (arg0 == this)
			return true;
		if (arg0 == null || arg0.getClass() != this.getClass())
			return false;
		Position oo = (Position)arg0;
		return (line == oo.line && col == oo.col && index == oo.index);
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return (line ^ col ^ index);
	}
	
	/**
	 * Returns a string representation of the position
	 * @return a string representation of the position
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public String toString()
	{
		return index+" ("+line+","+col+")";
	}
}
