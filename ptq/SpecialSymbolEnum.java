/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;

/**
 * A special symbol index enumerator.
 * Special symbols are not recognized symbols like the error symbol or the end/of/stream symbol
 * 
 * @author Alessio Scalici
 */
public enum SpecialSymbolEnum
{
	EOS (0),
	ERROR (1),
	BEGIN(2),
	END(3),
	UNDEFINED(-99);

	private final int code;

	SpecialSymbolEnum(int code)
	{
		this.code = code;
	}

	/**
	 * Returns the numeric value
	 * @return the numeric value
	 */
	public int toInt()
	{
		return code;
	}
	
	/**
	 * Returns the special symbol name
	 * @return the special symbol name
	 */
	public String toName()
	{
		return "_"+this.toString()+"_";
	}
	
	/**
	 * Returns the special symbol property
	 * @return the special symbol property
	 */
	public String toProperty()
	{
		return "-"+this.toString().toLowerCase()+"-symbol";
	}
	
	/**
	 * Returns the correct value, given the relative numeric code
	 * @param code the numeric code
	 * @return the value relative to the argument, UNDEFINED if code
	 * doesn't match any value
	 */
	public static SpecialSymbolEnum get(int code)
	{
		for (int i = 0; i < values().length; i++)
		{
			if (values()[i].code == code)
			{
				return values()[i];
			}
		}
		return UNDEFINED;
	}
	

}
