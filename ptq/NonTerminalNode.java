/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;

import java.util.LinkedList;
import java.util.List;

/**
 * A parse tree non-terminal node.
 * It can have one or more child (nodes without child are terminal).
 * The root of a parse tree is always a non-terminal node
 * @author Alessio Scalici
 */
public class NonTerminalNode extends AbstractNode
{
	// the list of node's children
	private LinkedList<AbstractNode> children;
	
	// the rule reduced to create this node
	private GrammarRule rule;
	
	{
		children = new LinkedList<AbstractNode>();
	}

	/**
	 * Creates a node 
	 * @param rule the rule reduced to create this node
	 * @throws NullPointerException if rule is null
	 */
	public NonTerminalNode(GrammarRule rule)
	{
		super(rule.getHead());

		this.rule = rule;
	}
	
	
	/**
	 * Returns the n-th child of this node
	 * @param n the child index
	 * @return the n-th child of this node
	 * @throws IndexOutOfBoundsException if n > getChildCount()-1
	 */
	public AbstractNode getChild(int n)
	{
		return children.get(n);
	}
	
	/**
	 * Returns a list containing the children of this node
	 * @return a list containing the children of this node
	 */
	public List<AbstractNode> getChildren()
	{
		return children;
	}
	
	/**
	 * Returns the number of children
	 * @return the number of children
	 */
	public int getChildCount()
	{
		return children.size();
	}
	
	/**
	 * Returns the non-terminal symbol this node represents (the rule's LHS)
	 * @return  the non-terminal symbol this node represents (the rule's LHS)
	 * @see ptq.AbstractNode#getSymbol()
	 */
	@Override
	public NonTerminalSymbol getSymbol()
	{
		return rule.getHead();
	}
	


	/**
	 * Returns the rule index
	 * @return the rule index
	 */
	public int getRuleIndex()
	{
		return rule.getIndex();
	}
	
	/**
	 * Returns the rule index
	 * @return the rule index
	 */
	public GrammarRule getRule()
	{
		return rule;
	}

	
	/**
	 * @see ptq.AbstractNode#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o)
	{
		if (!super.equals(o))
			return false;
		NonTerminalNode oo = (NonTerminalNode)o;

		return (this.rule.equals(oo.rule) && this.children.equals(oo.children));
	}
	
	
	/**
	 * @see ptq.AbstractNode#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return super.hashCode() ^ rule.hashCode() ^ children.hashCode();
	}
	
	/**
	 * @see ptq.AbstractNode#getPosition()
	 */
	@Override
	public Position getPosition()
	{
		if (children == null || children.size() == 0)
		{
			if (parent != null)
			{
				int index = parent.getChildren().indexOf(this);
				if (index < parent.getChildCount()-1)
					return parent.getChild(index +1).getPosition();
				return new Position();
			}
			else
				return new Position();
		}
			
		return children.peekFirst().getPosition();
	}
	
	
	/**
	 * Adds a child node in the first position
	 * @param c the node to add as a child
	 */
	public void addChildFirst(AbstractNode c)
	{
		children.addFirst(c);
		c.setParent(this);
	}
	
	/**
	 * Adds a child node in the last position
	 * @param c the node to add as a child
	 */
	public void addChildLast(AbstractNode c)
	{
		children.addLast(c);
		c.setParent(this);
	}
	
	
	/**
	 * Sets the rule reduced to create this node
	 * @param rule the rule reduced to create this node
	 */
	void setRule(GrammarRule rule)
	{
		if (rule == null)
			throw new NullPointerException();
		this.rule = rule;
		
	}


	/* (non-Javadoc)
	 * @see ptq3.AbstractNode#getLength()
	 */
	@Override
	public int getLength()
	{
		if (children == null || children.size() == 0 || getPosition() == null || children.get(children.size()-1).getPosition() == null)
			return 0;
		return children.get(children.size()-1).getPosition().getIndex() + children.get(children.size()-1).getLength() - getPosition().getIndex();
	}
	
	
	/**
	 * @see java.lang.Object#finalize()
	 */
	@Override
	public void finalize() throws Throwable
	{
		try
		{
			if (children != null)
				children.clear();
		}
		catch (Throwable t)
		{
			throw t;
		}
		finally
		{
			super.finalize();
		}
	}
}
