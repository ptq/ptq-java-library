/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;



/**
 * Exception throwned when a serialized file requires a greater library version
 * @author Alessio Scalici
 * @version 2013-05-06
 */
public class VersionNotSupportedException extends RuntimeException
{
	private static final long serialVersionUID = -3501646352343569L;
	
	private int requiredMajorVersion;
	private int requiredMinorVersion;
	private int requiredPatchVersion;
	private int actualMajorVersion;
	private int actualMinorVersion;
	private int actualPatchVersion;

	/**
	 * Creates a new VersionNotSupportedException
	 * @param reqMajor the required major version number
	 * @param reqMinor the required minor version number
	 * @param reqPatch the required patch version number
	 * @param actualMajor the library major version number
	 * @param actualMinor the library minor version number
	 * @param actualPatch the library patch version number
	 */
	VersionNotSupportedException(int reqMajor, int reqMinor, int reqPatch, int actualMajor, int actualMinor, int actualPatch)
	{
		super();
		requiredMajorVersion = reqMajor;
		requiredMinorVersion = reqMinor;
		requiredPatchVersion = reqPatch;
		actualMajorVersion = actualMajor;
		actualMinorVersion = actualMinor;
		actualPatchVersion = actualPatch;
	}

	
	/**
	 * Returns the required major version number
	 * @return the required major version number
	 */
	public int getRequiredMajorVersion()
	{
		return requiredMajorVersion;
	}

	/**
	 * Returns the required minor version number
	 * @return the required minor version number
	 */
	public int getRequiredMinorVersion()
	{
		return requiredMinorVersion;
	}

	/**
	 * Returns the required version
	 * @return the required version
	 */
	public String getRequiredVersion()
	{
		return requiredMajorVersion+"."+requiredMinorVersion+"."+requiredPatchVersion;
	}
	
	
	/**
	 * Returns the actual major version number
	 * @return the actual major version number
	 */
	public int getActualMajorVersion()
	{
		return actualMajorVersion;
	}

	/**
	 * Returns the actual minor version number
	 * @return the actual minor version number
	 */
	public int getActualMinorVersion()
	{
		return actualMinorVersion;
	}

	/**
	 * Returns the actual version
	 * @return the actual version
	 */
	public String getActualVersion()
	{
		return actualMajorVersion+"."+actualMinorVersion+"."+actualPatchVersion;
	}

	/**
	 * Returns the required patch version number
	 * @return the required patch version number
	 */
	public int getRequiredPatchVersion()
	{
		return requiredPatchVersion;
	}

	/**
	 * Returns the actual patch version number
	 * @return the actual patch version number
	 */
	public int getActualPatchVersion()
	{
		return actualPatchVersion;
	}
	
	

	

	
}
