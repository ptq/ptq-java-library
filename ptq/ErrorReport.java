/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;

import java.util.HashSet;
import java.util.Set;

/**
 * Contains informations about errors occurred during the recognition/parsing process.
 * Informations are: the error type, the last token recognized by the scanner, a list of expected symbols
 * (if syntax error)
 * 
 * @author Alessio Scalici
 */
public class ErrorReport 
{
	// the error type
	private ErrorType type;
	
	// the last recognized token
	private TerminalNode lastToken;
	
	// a list of expected symbols  (if syntax error)
	private Set<TerminalSymbol> expectedSymbols;
	
	
	/**
	 * Builds an error report
	 * @param type the error type
	 * @param lastToken the last recognized token
	 * @param expectedSymbols a list of expected symbols (if syntax error)
	 */
	ErrorReport(ErrorType type, TerminalNode lastToken, Set<TerminalSymbol> expectedSymbols) 
	{
		super();
		this.type = type;
		this.lastToken = lastToken;
		this.expectedSymbols = expectedSymbols;
	}
	
	/**
	 * Builds an error report
	 * @param type the error type
	 * @param lastToken the last recognized token
	 */
	ErrorReport(ErrorType type, TerminalNode lastToken) 
	{
		super();
		this.type = type;
		this.lastToken = lastToken;
		this.expectedSymbols = null;
	}

	/**
	 * Returns the error type
	 * @return the error type
	 */
	public ErrorType getType() 
	{
		return type;
	}

	/**
	 * Returns the last recognized token
	 * @return the last recognized token
	 */
	public TerminalNode getLastToken() 
	{
		return lastToken;
	}

	/**
	 * If this is a syntax error, returns the list of expected symbols, otherwise returns null
	 * @return the list of expected symbols if syntax error, null otherwise
	 */
	public Set<TerminalSymbol> getExpectedSymbols() 
	{
		return expectedSymbols;
	}

	/**
	 * Adds an expected symbol
	 * @param arg0 the symbol to add
	 * @return true if the symbol was added
	 * @see java.util.ArrayList#add(java.lang.Object)
	 */
	boolean add(TerminalSymbol arg0) 
	{
		if (this.expectedSymbols == null)
			this.expectedSymbols = new HashSet<TerminalSymbol>();
		return expectedSymbols.add(arg0);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		String res = (this.type == null ? "null" : this.type.toString());
		if (this.lastToken != null && this.lastToken.getPosition() != null)
			res += " ("+this.lastToken.getPosition().getLine()+","+this.lastToken.getPosition().getCol()+")";
		if (this.expectedSymbols != null && !this.expectedSymbols.isEmpty())
		{
			res += " ->";
			for (TerminalSymbol s : expectedSymbols)
				res += " " + s.getName();
		}
		return res;
	}
	
	@Override
	public void finalize() throws Throwable
	{
		try
		{
			if (expectedSymbols != null)
				expectedSymbols.clear();
		}
		catch (Throwable t)
		{
			throw t;
		}
		finally
		{
			super.finalize();
		}
	}
	
}
