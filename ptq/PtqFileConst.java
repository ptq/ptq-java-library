/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;

/**
 * A collection of control bytes used in serialized objects
 * 
 * @author Alessio Scalici
 */
class PtqFileConst
{
	static final int BYTE_RECORD_SEPARATOR = 30;
	static final int BYTE_UNIT_SEPARATOR = 31;
	
	static final int BEGIN_INFO_SECTION = 73;				// I
	static final int BEGIN_SCANNER_SECTION = 83;				// S
	static final int BEGIN_CHARACTER_SET_SECTION = 67;		// C
	static final int BEGIN_TERMINAL_SECTION = 84;			// T
	static final int BEGIN_DFA_SECTION = 68;					// D
	static final int BEGIN_SINGLE_DFA = 68;					// d
	static final int BEGIN_DFA_STATE_SECTION = 115;			// s
	static final int BEGIN_DFA_EDGE_SECTION = 101;			// e
	static final int BEGIN_DFA_ACTION_SECTION = 97;			// a
	
	static final int DFA_ACTION_IGNORE = 105;				// i
	static final int DFA_ACTION_NEWLINE = 110;				// n
	static final int DFA_ACTION_PUSHBACK = 112;				// p
	static final int DFA_ACTION_BEGIN = 115;				// s
	static final int DFA_ACTION_END = 101;					// e
	static final int DFA_ACTION_GOTO = 103;					// g
	static final int DFA_ACTION_INDENT = 104;				// h
	
	// Parser
	
	static final int BEGIN_PARSER_SECTION = 80;				// P
	
	static final int PARSER_TYPE_LR0 = 121;					// y
	static final int PARSER_TYPE_SLR = 120;					// x
	static final int PARSER_TYPE_LR1 = 119;					// w
	static final int PARSER_TYPE_LALR1 = 118;				// v
	static final int PARSER_TYPE_LRED1 = 117;				// u
	
	static final int BEGIN_SYMBOL_SECTION = 83;				// S
	static final int BEGIN_RULE_SECTION = 82;				// R
	static final int BEGIN_TABLE_SECTION = 84;				// T
	
}
