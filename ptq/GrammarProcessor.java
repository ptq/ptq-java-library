/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;


import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

class GrammarProcessor
{
	
	/**
	 * Used for parsing error reports
	 * @param ruleTable the grammar rule table
	 * @param symbolTable the symbol table
	 * @return the First Sets map
	 */
	static Map<AbstractSymbol, Set<TerminalSymbol>> processFirstSets(GrammarRule[] ruleTable, AbstractSymbol[] symbolTable)
	{
		int iFirstNonTerminal = 2;
		for (AbstractSymbol s : symbolTable)
		{
			if (s instanceof NonTerminalSymbol)
			{
				iFirstNonTerminal = s.getIndex();
				break;
			}
		}
		int nTerm = iFirstNonTerminal;
		int nNonTerm = symbolTable.length-iFirstNonTerminal;

		Map<AbstractSymbol, Set<TerminalSymbol>> fiSets = new HashMap<AbstractSymbol, Set<TerminalSymbol>>(nTerm+nNonTerm);
		

		TerminalSymbol epsilon = new TerminalSymbol("EPSILON", symbolTable.length);
		// for every symbol, initialize an empty set
	
		for (int i=0; i<symbolTable.length; ++i)
			fiSets.put(symbolTable[i], new HashSet<TerminalSymbol>());
		fiSets.put(epsilon, new HashSet<TerminalSymbol>());
		
		
		fiSets.get(epsilon).add(epsilon);
		
		for (int i=0; i<iFirstNonTerminal; ++i) // STEP 1 (terminals)
			fiSets.get(symbolTable[i]).add((TerminalSymbol)symbolTable[i]);
		
		for (int i=iFirstNonTerminal; i<symbolTable.length; ++i) // STEP 2 (non-terminals)
		{
			for (GrammarRule r : ruleTable)
				if (symbolTable[i].equals(r.getHead()))
					if (r.isEpsilon())
						fiSets.get(symbolTable[i]).add(epsilon);
		}
		
		boolean changed = true; // STEP 3
		while (changed)
		{
			changed = false;
			
			for (int i=iFirstNonTerminal; i<symbolTable.length; ++i)
			{
				for (GrammarRule r : ruleTable)
				{
					if (symbolTable[i].equals(r.getHead()))
					{
						for (AbstractSymbol s : r.getTail())
						{
							if (fiSets.get(symbolTable[i]).addAll(fiSets.get(s)))
								changed = true;
							if (!fiSets.get(s).contains(epsilon))
								break;
						}
					}
				}
			}
		}
		
		return fiSets;
	}
}
