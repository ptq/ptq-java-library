/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;



import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;


/**
 * Provides methods to deserialize a scanner
 * 
 * @author Alessio Scalici
 */
public class ScannerDeserializer extends BaseDeserializer
{
	// contains the character set collection
	static private ArrayList<TreeSet<Integer>> characterSets;
	
	// contains the symbol table
	static private TerminalSymbol[] symbols;
	
	// contains the scanner dfas
	static private ArrayList<BaseSubScanner> dfas;
	
	// contains the dfa actions that requires a target for later linking
	static private ArrayList<int[]> targetActionList;

	// contains number of non-ignored symbols
	static private int publicSymbolTableLength;
	
	static private boolean containsIndentFlag;
	
	/**
	 * Deserialize a complete scanner stream (with header information)
	 * @param is the scanner stream
	 * @return the resulting scanner
	 * @throws IOException if an I/O error occurs
	 * @throws DeserializationException if something goes wrong with the stream format
	 * @throws VersionNotSupportedException if the serialized file scanner requires a greater Ptq library version
	 */
	public static BaseScanner deserialize(InputStream is) throws IOException, DeserializationException, VersionNotSupportedException
	{
		ScannerDeserializer.inputStream = is;
		readHeader();
		readInfoSection();
		
		BaseScanner res = readScannerSection();
		res.setName(name);
		return res;
	}
	
	/**
	 * Deserialize scanner from stream (without header information), used by parser deserializer
	 * @param inputStream the input stream
	 * @return the resulting scanner
	 * @throws IOException if an I/O error occurs
	 * @throws DeserializationException if something goes wrong with the stream format
	 */
	static BaseScanner deserializeSection(InputStream bb) throws IOException, DeserializationException
	{
		inputStream = bb;
		
		BaseScanner res = readScannerSection();

		return res;
	}

	
	
	
	/**
	 * Reads the scanner section
	 * @return the scanner
	 * @throws IOException if an I/O error occurs
	 * @throws DeserializationException if the stream format is incorrect
	 */
	static private BaseScanner readScannerSection() throws IOException, DeserializationException
	{
		int c = readHTF8();
		if (c != PtqFileConst.BEGIN_SCANNER_SECTION)
			throw new DeserializationException("Error reading Scanner section begin (expected "+PtqFileConst.BEGIN_SCANNER_SECTION+", read "+c+")");
		
		readCharacterSetSection();
		readTerminalSection();
		readDfaSection();
		
		c = readHTF8();
		if (c != PtqFileConst.BYTE_RECORD_SEPARATOR)
			throw new DeserializationException("Error reading Scanner section end (expected "+PtqFileConst.BYTE_RECORD_SEPARATOR+", read "+c+")");
		
		BaseScanner scan;
		if (containsIndentFlag)
			scan = new IndentBasedScanner(dfas, dfas.get(0).getName(), symbols);
		else
			scan = new BaseScanner(dfas, dfas.get(0).getName(), symbols);
		
		scan.setFirstIgnoredIndex(publicSymbolTableLength);
		return scan;
	}
	
	/**
	 * Reads the character set section
	 * @throws IOException if an I/O error occurs
	 * @throws DeserializationException if the stream format is incorrect
	 */
	static private void readCharacterSetSection() throws IOException, DeserializationException
	{
		int c = readHTF8();
		if (c != PtqFileConst.BEGIN_CHARACTER_SET_SECTION)
			throw new DeserializationException("Error reading Character Set section begin (expected "+PtqFileConst.BEGIN_CHARACTER_SET_SECTION+", read "+c+")");
		

		int iSetCount = readHTF8();
		
		c = readHTF8();
		if (c != PtqFileConst.BYTE_UNIT_SEPARATOR)
			throw new DeserializationException("Error reading Character Set section Unit Separator (expected "+PtqFileConst.BYTE_UNIT_SEPARATOR+", read "+c+")");

		characterSets = new ArrayList<TreeSet<Integer>>(iSetCount);
		
		for (int i=0; i<iSetCount; ++i)
		{
			int iRangeCount = readHTF8();
			
			c = readHTF8();
			if (c != PtqFileConst.BYTE_UNIT_SEPARATOR)
				throw new DeserializationException("Error reading Character Set section Unit Separator (expected "+PtqFileConst.BYTE_UNIT_SEPARATOR+", read "+c+")");
			
			TreeSet<Integer> curSet = new TreeSet<Integer>();
			
			for (int j=0; j<iRangeCount; ++j)
			{
				int iStart = readHTF8();
				int iEnd = readHTF8();
				
				for (int k=iStart; k<=iEnd; ++k)
				{
					if (k<0 || k>0x10FFFF || (k>=0xD800 && k<= 0xDFFF))
						throw new DeserializationException("Error reading Character Set section: invalid Unicode code point 0x"+Integer.toHexString(k));
					curSet.add(k);
				}
			}
			characterSets.add(curSet);
		}
		
	}
	
	/**
	 * Reads the terminal table section
	 * @throws IOException if an I/O error occurs
	 * @throws DeserializationException if the stream format is incorrect
	 */
	static private void readTerminalSection() throws IOException, DeserializationException
	{
		int c = readHTF8();
		if (c != PtqFileConst.BEGIN_TERMINAL_SECTION)
			throw new DeserializationException("Error reading Terminal section begin (expected "+PtqFileConst.BEGIN_TERMINAL_SECTION+", read "+c+")");

		int iCount = readHTF8();
		
		c = readHTF8();
		if (c != PtqFileConst.BYTE_UNIT_SEPARATOR)
			throw new DeserializationException("Error reading Terminal section Unit Separator (expected "+PtqFileConst.BYTE_UNIT_SEPARATOR+", read "+c+")");

		
		
		publicSymbolTableLength = readHTF8();
		
		c = readHTF8();
		if (c != PtqFileConst.BYTE_UNIT_SEPARATOR)
			throw new DeserializationException("Error reading Terminal section Unit Separator (expected "+PtqFileConst.BYTE_UNIT_SEPARATOR+", read "+c+")");

		
		
		symbols = new TerminalSymbol[iCount];
		
		String aux;
		for (int i=0; i<iCount; ++i)
		{
			symbols[i] = new TerminalSymbol(readString(), i);
			aux = readString();
			if (aux.length() != 0)
				symbols[i].setDisplayName(aux);
		}
	}
	
	/**
	 * Reads the DFAs section
	 * @throws IOException if an I/O error occurs
	 * @throws DeserializationException if the stream format is incorrect
	 */
	static private void readDfaSection() throws IOException, DeserializationException
	{
		int c = readHTF8();
		if (c != PtqFileConst.BEGIN_DFA_SECTION)
			throw new DeserializationException("Error reading DFA section begin (expected "+PtqFileConst.BEGIN_DFA_SECTION+", read "+c+")");

		int iCount = readHTF8();
		
		c = readHTF8();
		if (c != PtqFileConst.BYTE_UNIT_SEPARATOR)
			throw new DeserializationException("Error reading DFA section Unit Separator (expected "+PtqFileConst.BYTE_UNIT_SEPARATOR+", read "+c+")");

		dfas = new ArrayList<BaseSubScanner>(iCount);
		
		targetActionList = new ArrayList<int[]>();
		
		for (int i=0; i<iCount; ++i)
			dfas.add(readSingleDfa());
		

		for (int[] ar : targetActionList)
		{
			switch (ar[1])
			{
				case PtqFileConst.DFA_ACTION_BEGIN:
					dfas.get(ar[0]).upsertAction(symbols[ar[2]]).setStart(dfas.get(ar[3]));
					break;
				case PtqFileConst.DFA_ACTION_GOTO:
					dfas.get(ar[0]).upsertAction(symbols[ar[2]]).setGoto(dfas.get(ar[3]));
					break;
				default:
					break;
			}
		}
	}
	
	/**
	 * Reads a DFA section
	 * @throws IOException if an I/O error occurs
	 * @throws DeserializationException if the stream format is incorrect
	 */
	static private BaseSubScanner readSingleDfa() throws IOException, DeserializationException
	{
		int c = readHTF8();
		if (c != PtqFileConst.BEGIN_SINGLE_DFA)
			throw new DeserializationException("Error reading DFA begin (expected "+PtqFileConst.BEGIN_SINGLE_DFA+", read "+c+")");

		String name = readString();
	
		// read state subsection
		c = readHTF8();
		if (c != PtqFileConst.BEGIN_DFA_STATE_SECTION)
			throw new DeserializationException("Error reading DFA states begin (expected "+PtqFileConst.BEGIN_DFA_STATE_SECTION+", read "+c+")");

		int iCountStates = readHTF8();
		
		c = readHTF8();
		if (c != PtqFileConst.BYTE_UNIT_SEPARATOR)
			throw new DeserializationException("Error reading DFA section Unit Separator (expected "+PtqFileConst.BYTE_UNIT_SEPARATOR+", read "+c+")");


		ArrayList<BaseScannerState> states = new ArrayList<BaseScannerState>(iCountStates);
		for (int i=0; i<iCountStates; ++i)
			states.add(new BaseScannerState());

		int iCountFinal = readHTF8();
		
		c = readHTF8();
		if (c != PtqFileConst.BYTE_UNIT_SEPARATOR)
			throw new DeserializationException("Error reading DFA section Unit Separator (expected "+PtqFileConst.BYTE_UNIT_SEPARATOR+", read "+c+")");

		for (int i=0; i<iCountFinal; ++i)
		{
			int iStateIndex = readHTF8();
			int iSymbolIndex = readHTF8();
			states.get(iStateIndex).setAcceptedSymbol(symbols[iSymbolIndex]);
		}
		
		// read edge subsection
		c = readHTF8();
		if (c != PtqFileConst.BEGIN_DFA_EDGE_SECTION)
			throw new DeserializationException("Error reading DFA edge begin (expected "+PtqFileConst.BEGIN_DFA_EDGE_SECTION+", read "+c+")");

		int iCountEdges = readHTF8();
		
		c = readHTF8();
		if (c != PtqFileConst.BYTE_UNIT_SEPARATOR)
			throw new DeserializationException("Error reading DFA section Unit Separator (expected "+PtqFileConst.BYTE_UNIT_SEPARATOR+", read "+c+")");
	
		ArrayList<ArrayList<Edge>> edges = new ArrayList<ArrayList<Edge>>(iCountStates);
		for (int i=0; i<iCountStates; ++i)
			edges.add(new ArrayList<Edge>());
		for (int i=0; i<iCountEdges; ++i)
		{	
			int iSrc = readHTF8();
			int iDst = readHTF8();
			int iSet = readHTF8();
			
			edges.get(iSrc).add(new Edge(states.get(iSrc), states.get(iDst), characterSets.get(iSet)));
			
		}
		
		for (int i=0; i<iCountStates; ++i)
		{
			int iCurEdgeNum = edges.get(i).size();
			Edge[] eds = new Edge[iCurEdgeNum];
			for (int j=0; j<iCurEdgeNum; ++j)
				eds[j] = edges.get(i).get(j);
			states.get(i).setEdges(eds);
		}
		
		
		
		// read action subsection
		c = readHTF8();
		if (c != PtqFileConst.BEGIN_DFA_ACTION_SECTION)
			throw new DeserializationException("Error reading DFA action begin (expected "+PtqFileConst.BEGIN_DFA_ACTION_SECTION+", read "+c+")");

		int iCountActions = readHTF8();
		
		c = readHTF8();
		if (c != PtqFileConst.BYTE_UNIT_SEPARATOR)
			throw new DeserializationException("Error reading DFA section Unit Separator (expected "+PtqFileConst.BYTE_UNIT_SEPARATOR+", read "+c+")");
	
		Action act = null;
		HashMap<TerminalSymbol, Action> actMap = new HashMap<TerminalSymbol, Action>();
		boolean indentFlag = false;
		for (int i=0; i<iCountActions; ++i)
		{
			int iType = readHTF8();
			int iSymbolIndex = readHTF8();
			int iTarget;
			
			if (actMap.containsKey(symbols[iSymbolIndex]))
				act = actMap.get(symbols[iSymbolIndex]);
			else
			{
				act = new Action(symbols[iSymbolIndex]);
				actMap.put(symbols[iSymbolIndex], act);
			}
			
			switch (iType)
			{
				case PtqFileConst.DFA_ACTION_IGNORE:
					act.setIgnore(true);
					break;
					
				case PtqFileConst.DFA_ACTION_PUSHBACK:
					act.setPushback(true);
					break;
					
				case PtqFileConst.DFA_ACTION_END:
					act.setEnd(true);
					break;
					
				case PtqFileConst.DFA_ACTION_INDENT:
					act.setIndent(true);
					indentFlag = true;
					containsIndentFlag = true;
					break;
					
				case PtqFileConst.DFA_ACTION_BEGIN:
				case PtqFileConst.DFA_ACTION_GOTO:
					iTarget = readHTF8();
	
					int[] arAction = new int[4];
					arAction[0] = dfas.size(); // dfa index
					arAction[1] = iType;			// action type
					arAction[2] = iSymbolIndex;		// symbol index
					arAction[3] = iTarget;			// action target
					targetActionList.add(arAction);
					break;
					
				default:
					break;
			}
		}
		
		BaseSubScanner res;
		if (indentFlag)
			res = new IndentBasedSubScanner(name, states.get(0));
		else
			res = new BaseSubScanner(name, states.get(0));
		
		for (Action a : actMap.values())
			res.setAction(a);
			
		
		return res;
		
	}
	
	
	
	
	

	
}
