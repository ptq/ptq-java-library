/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;

import java.io.IOException;
import java.io.InputStream;

public class HexStringInputStream extends InputStream
{

	private String src;
	private int i = 0;
	
	public HexStringInputStream(String src)
	{
		if (src == null)
			throw new NullPointerException();
		this.src = src;
	}
	
	
	@Override
	public int read() throws IOException
	{
		if (i<src.length()-1)
		{
			int res = Integer.valueOf(src.substring(i, i+2), 16)+ Byte.MIN_VALUE;
			i += 2;
			return res;
		}
		return -1;
	}


	@Override
	public synchronized void reset() throws IOException
	{
		super.reset();
		i=0;
	}
	
	

}
