/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;


import java.util.List;


/**
 * A rule (production) of a context-free grammar
 * 
 * @author Alessio Scalici
 */
public class GrammarRule
{
	// the index of the rule, this must be unique among rules in the same grammar
	private int index;
	
	// the rule left-hand side
	private NonTerminalSymbol head;
	
	// the rule right-hand side
	private List<AbstractSymbol> tail;

	

	
	/**
	 * Builds a grammar rule
	 * @param index the rule's index
	 * @param head the rule's LHS
	 * @param tail the rule's RHS
	 * @throws NullPointerException if head or tail are null
	 * @throws IllegalArgumentException if index < 0
	 */
	GrammarRule(int index, NonTerminalSymbol head, List<AbstractSymbol> tail) throws NullPointerException, IllegalArgumentException
	{
		super();
		if (head == null || tail == null)
			throw new NullPointerException();
		if (index < 0)
			throw new IllegalArgumentException();
		this.index = index;
		this.head = head;
		this.tail = tail;
	}


	/**
	 * Returns the rule's index
	 * @return the rule's index
	 */
	public int getIndex()
	{
		return index;
	}

	/**
	 * Returns the rule's LHS
	 * @return the rule's LHS
	 */
	public NonTerminalSymbol getHead()
	{
		return head;
	}

	/**
	 * Returns the rule's RHS
	 * @return the rule's RHS
	 */
	public List<AbstractSymbol> getTail()
	{
		return tail;
	}
	
	/**
	 * Returns true if this is an epsilon-rule (or lambda-rule), false otherwise
	 * @return true if this is an epsilon-rule (or lambda-rule), false otherwise
	 */
	boolean isEpsilon()
	{
		return (tail.size() == 0);
	}
	
	@Override
	public String toString()
	{
		StringBuffer res = new StringBuffer("<"+head.getName()+"> ::= ");
		for (AbstractSymbol s : tail)
			if (s instanceof TerminalSymbol)
				res.append(s.getName()+" ");
			else
				res.append("<"+s.getName()+"> ");
		return res.toString();
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (o == this)
			return true;
		if (o == null || o.getClass() != this.getClass())
			return false;
		GrammarRule oo = (GrammarRule)o;
		if (this.head.equals(oo.head) && this.tail.equals(oo.tail))
			return true;
		return false;
	}
	
	@Override
	public int hashCode()
	{
		return head.hashCode() ^ tail.hashCode();
	}
	
	@Override
	public void finalize() throws Throwable
	{
		try
		{
			if (tail != null)
				tail.clear();
		}
		catch (Throwable t)
		{
			throw t;
		}
		finally
		{
			super.finalize();
		}
	}
	
}
