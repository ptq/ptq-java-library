/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;

import java.io.IOException;
import java.io.InputStream;

/**
 * This class provides common fields and methods to stream
 * deserializers (e.g. to make a scanner or parser from a file)
 * 
 * @author Alessio Scalici
 */
class BaseDeserializer
{
	
	/**
	 * The major version required by the serialized parser (or scanner)
	 */
	static protected int iRequiredMajorVersion;
	/**
	 * The minor version required by the serialized parser (or scanner)
	 */
	static protected int iRequiredMinorVersion;
	
	/**
	 * The patch version required by the serialized parser (or scanner)
	 */
	static protected int iRequiredPatchVersion;
	
	/**
	 * The project name
	 */
	static protected String name;
	
	/**
	 * The input stream read by the deserializer
	 */
	static protected InputStream inputStream;
	
	
	public static InputStream getInputStream()
	{
		return inputStream;
	}

	public static void setInputStream(InputStream is)
	{
		inputStream = is;
	}

	/**
	 * Reads the application stream fixed header
	 * @throws IOException
	 * @throws DeserializationException if the header isn't correct
	 */
	static protected void readHeader() throws IOException, DeserializationException
	{
		byte[] buf = new byte[4];
		if (inputStream.read(buf) != 4)
			throw new DeserializationException("Error reading header");
		
		if (buf[0] != 80 || buf[1] != 84 || buf[2] != 81 || buf[3] != 30)
			throw new DeserializationException("Error reading header");
	}
	
	/**
	 * Reads the application informations section
	 * @throws IOException
	 * @throws VersionNotSupportedException if a greater version is required
	 * @throws DeserializationException if the stream format is incorrect
	 */
	static protected void readInfoSection() throws IOException, VersionNotSupportedException, DeserializationException
	{
		int c = readHTF8();
		if (c != PtqFileConst.BEGIN_INFO_SECTION)
			throw new DeserializationException("Error reading Info section begin (expected "+PtqFileConst.BEGIN_INFO_SECTION+", read "+c+")");
		
		
		name = readString();
	
		c = readHTF8();
		if (c != PtqFileConst.BYTE_RECORD_SEPARATOR)
			throw new DeserializationException("Error reading Info section end (expected "+PtqFileConst.BYTE_RECORD_SEPARATOR+", read "+c+")");

		
		iRequiredMajorVersion = readHTF8();
		iRequiredMinorVersion = readHTF8();
		iRequiredPatchVersion = readHTF8();
		
		if ((iRequiredMajorVersion > PtqLib.MAJOR_VERSION)
				|| (iRequiredMajorVersion == PtqLib.MAJOR_VERSION && iRequiredMinorVersion > PtqLib.MINOR_VERSION)
				|| (iRequiredMajorVersion == PtqLib.MAJOR_VERSION && iRequiredMinorVersion == PtqLib.MINOR_VERSION && iRequiredPatchVersion > PtqLib.PATCH_VERSION)
				)
			throw new VersionNotSupportedException(iRequiredMajorVersion, iRequiredMinorVersion, iRequiredPatchVersion, PtqLib.MAJOR_VERSION, PtqLib.MINOR_VERSION, PtqLib.PATCH_VERSION);
		
		c = readHTF8();
		if (c != PtqFileConst.BYTE_RECORD_SEPARATOR)
			throw new DeserializationException("Error reading Info section end (expected "+PtqFileConst.BYTE_RECORD_SEPARATOR+", read "+c+")");
	}
	
	
	/**
	 * Reads a positive integer in HTF8 format
	 * @throws IOException
	 * @throws DeserializationException if the integer format is incorrect
	 */
	static protected int readHTF8() throws IOException, DeserializationException
	{
		byte b = (byte)inputStream.read();
		if ((b & 0x80) == 0)								
			return b;
		
		if ((b & 0x40) == 0) // error 
			throw new DeserializationException("Stream format error: unexpected continuation byte");
		
		int res = 0;
		byte mask0 = 0x20;
		byte mask1 = 0x1F;
		for (int mult = 1; mult < 6; ++mult)
		{
			res = (res << 6) | (inputStream.read() & 0x3F);
			if ((b & mask0) == 0)
				return res | ((b & mask1) << (6*mult));
			
			mask0 >>>= 1;
			mask1 >>>= 1;
		}

		throw new DeserializationException("Illegal start byte: "+Integer.toHexString(b));
	}
	
	/**
	 * Reads an ASCII string
	 * @throws IOException
	 * @throws DeserializationException 
	 */
	static protected String readString() throws IOException, DeserializationException
	{
		int size = readHTF8();
		byte[] buf = new byte[size];
		if (inputStream.read(buf) != size)
			throw new DeserializationException();
		return new String(buf, "ASCII");
	}
}
