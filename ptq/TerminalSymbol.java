/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;


/**
 * A terminal symbol.
 * 
 * @author Alessio Scalici
 */
public class TerminalSymbol extends AbstractSymbol
{
	
	// the symbol display description
	private String display;
	
	/**
	 * Builds a terminal symbol
	 * @param name the symbol name
	 * @param index the symbol index
	 * @throws NullPointerException if name is null
	 * @throws IllegalArgumentException if index < 0
	 */
	public TerminalSymbol(String name, int index) throws NullPointerException, IllegalArgumentException
	{
		super(name, index);
		this.display = null;
	}

	/**
	 * Sets the display name
	 * @param disp the new display name
	 */
	public void setDisplayName(String disp)
	{
		this.display = disp;
	}
	
	/**
	 * Gets the display name
	 * @return the display name
	 */
	public String getDisplayName()
	{
		if (this.display == null)
			return getName();
		return this.display;
	}


}
