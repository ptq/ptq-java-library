/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;



import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;



/**
 * An implementation of a LR1 parser.
 * It uses an action-goto table and a stack to implement the recognition
 * automaton. It can recognize LR1 grammars and build a parse tree
 * 
 * @author Alessio Scalici
 */
public class Parser
{
	// the action-goto table
	private LRAction[][] lrTable;
	
	// the rule table
	private GrammarRule[] ruleTable;
	
	// the symbol table
	private AbstractSymbol[] symbolTable;

	// the PDA stack
	private Stack<Integer> stateStack;
	
	// the source scanner
	private Scanner scanner;
	
	// the start symbol index
	private int startIndex;
	
	// the error report
	private ErrorReport error;
	
	
	/**
	 * Builds a LR1 parser
	 * @param lrTable the action-goto table
	 * @param ruleTable the grammar rule table
	 * @param symbolTable the symbol table
	 * @param startIndex the root symbol index
	 * @param scanner the source scanner
	 */
	Parser(LRAction[][] lrTable, GrammarRule[] ruleTable,
			AbstractSymbol[] symbolTable, int startIndex,
			Scanner scanner)
	{
		super();
		this.lrTable = lrTable;
		this.ruleTable = ruleTable;
		this.symbolTable = symbolTable;
		this.scanner = scanner;
		this.stateStack = new Stack<Integer>();
		this.startIndex = startIndex;
		this.stateStack.push(SpecialSymbolEnum.EOS.toInt());
	}
	

	
	/**
	 * Resets the parser and the source scanner
	 * @throws IOException if an I/O error occurs
	 */
	public void reset() throws IOException
	{

		// XXX reduce
		this.curNodeStack.clear();
		// XXX end reduce
		this.stateStack.clear();
		this.stateStack.push(SpecialSymbolEnum.EOS.toInt());
		
		error = null;
		scanner.reset();
	}
	
	
	// GESTIRE BENE LE DIMENSIONI DEI BUFFER
	public void setSource(InputStream is, Charset cs, int fileBufferSize)
	{
		((BaseScanner)this.scanner).setSource(is, cs, fileBufferSize);
	}
	
	/**
	 * Returns the source scanner
	 * @return the source scanner
	 */
	
	public Scanner getScanner()
	{
		return scanner;
	}
	
	/**
	 * Returns the error report, if no error occurred returns null
	 * @return the error report, if no error occurred returns null
	 */
	
	public ErrorReport getErrorReport()
	{
		return error;
	}
	
	/**
	 * Returns the parse tree root node, or null if an encoding/lexical/syntax error occurred
	 * @param reader the source reader
	 * @return the parse tree root node, or null if an encoding/lexical/syntax error occurred
	 */
	
	public NonTerminalNode parse(Reader reader) throws IOException
	{
		this.reset();
		scanner.setSource(reader);
		return parse();
	}
	
	/**
	 * Returns the parse tree root node, or null if an encoding/lexical/syntax error occurred
	 * @param s the source input stream
	 * @return the parse tree root node, or null if an encoding/lexical/syntax error occurred
	 */
	
	public NonTerminalNode parse(InputStream s) throws IOException
	{
		this.reset();
		scanner.setSource(s);
		return parse();
	}
	
	/**
	 * Returns the parse tree root node, or null if an encoding/lexical/syntax error occurred
	 * @param s the source file
	 * @return the parse tree root node, or null if an encoding/lexical/syntax error occurred
	 */
	
	public NonTerminalNode parse(File s)  throws IOException, FileNotFoundException
	{
		this.reset();
		scanner.setSource(s);
		return parse();
	}
	
	/**
	 * Returns the parse tree root node, or null if an encoding/lexical/syntax error occurred
	 * @param s the source string
	 * @return the parse tree root node, or null if an encoding/lexical/syntax error occurred
	 */
	public NonTerminalNode parse(String s) throws IOException
	{
		this.reset();
		scanner.setSource(new StringReader(s));
		return parse();
	}
	
	/**
	 * Returns the parse tree root node, or null if an encoding/lexical/syntax error occurred
	 * @return the parse tree root node, or null if an encoding/lexical/syntax error occurred
	 * @throws IOException if an I/O error occurs
	 * @throws IllegalStateException  if the source is not ready
	 */
	public NonTerminalNode parse() throws IOException, IllegalStateException
	{
		if (!this.scanner.hasSource())
			throw new IllegalStateException();
		
		Stack<AbstractNode> pt = new Stack<AbstractNode>();
		TerminalNode tok = null;
		while (true)
		{
			if (tok == null)
				tok = this.scanner.scan();
			
			if (tok.getSymbol().getIndex() == SpecialSymbolEnum.ERROR.toInt())
			{
				error = new ErrorReport(ErrorType.LEXICAL_ERROR, tok);
				return null;
			}
			
			LRAction act = lrTable[stateStack.peek()][tok.getSymbol().getIndex()];
			if (act == null)
			{
				Map<AbstractSymbol, Set<TerminalSymbol>> map = GrammarProcessor.processFirstSets(ruleTable, symbolTable);
				Set<TerminalSymbol> set = new HashSet<TerminalSymbol>();
				for (int i=0; i<lrTable[stateStack.peek()].length; ++i)
					if (lrTable[stateStack.peek()][i] != null && lrTable[stateStack.peek()][i].getType() != LRActionType.NONE) 
						set.addAll(map.get(symbolTable[i]));
				
				error = new ErrorReport(ErrorType.SYNTAX_ERROR, tok, set);
				return null;
			}
			switch (act.getType())
			{
				case SHIFT:
					
					stateStack.push(act.getTargetIndex());
					
					pt.push(tok);
					
					tok = null;
					break;
				case REDUCE:
					
					GrammarRule rule = ruleTable[act.getTargetIndex()];
					int state = 0;
					
					NonTerminalNode nt = new NonTerminalNode(rule);
					for (int i=0; i<rule.getTail().size(); ++i)
					{
						state = stateStack.pop();
						nt.addChildFirst(pt.pop());
					}
					pt.push(nt);
					state = stateStack.peek();
					stateStack.push(lrTable[state][rule.getHead().getIndex()].getTargetIndex());
					
					break;
				case GOTO:
					// do nothing
					break;
				case ACCEPT:
					return (NonTerminalNode)pt.peek();
					
				case NONE:
					
				default: 
					Map<AbstractSymbol, Set<TerminalSymbol>> map = GrammarProcessor.processFirstSets(ruleTable, symbolTable);
					Set<TerminalSymbol> set = new HashSet<TerminalSymbol>();
					for (int i=0; i<lrTable[stateStack.peek()].length; ++i)
						if (lrTable[stateStack.peek()][i] != null && lrTable[stateStack.peek()][i].getType() != LRActionType.NONE) 
							set.addAll(map.get(symbolTable[i]));
					
					error = new ErrorReport(ErrorType.SYNTAX_ERROR, tok, set);
					return null;
			}
		}	
	}
	
	
	// XXX reduce
	public enum ReduceResult{ READY, ACCEPT, ERROR }
	
	private Stack<AbstractNode> curNodeStack = new Stack<AbstractNode>();
	
	public NonTerminalNode getCurrentNode()
	{
		if (curNodeStack == null)
			return null;
		return (NonTerminalNode)curNodeStack.peek();
	}
	
	public ReduceResult reduce() throws IOException, IllegalStateException
	{
		if (!this.scanner.hasSource())
			throw new IllegalStateException();
		
		TerminalNode tok = null;
		while (true)
		{
			if (tok == null)
				tok = this.scanner.scan();
			
			if (tok.getSymbol().getIndex() == SpecialSymbolEnum.ERROR.toInt())
			{
				error = new ErrorReport(ErrorType.LEXICAL_ERROR, tok);
				return ReduceResult.ERROR;
			}
			
			LRAction act = lrTable[stateStack.peek()][tok.getSymbol().getIndex()];
			if (act == null)
			{
				Map<AbstractSymbol, Set<TerminalSymbol>> map = GrammarProcessor.processFirstSets(ruleTable, symbolTable);
				Set<TerminalSymbol> set = new HashSet<TerminalSymbol>();
				for (int i=0; i<lrTable[stateStack.peek()].length; ++i)
					if (lrTable[stateStack.peek()][i] != null && lrTable[stateStack.peek()][i].getType() != LRActionType.NONE) 
						set.addAll(map.get(symbolTable[i]));
				
				error = new ErrorReport(ErrorType.SYNTAX_ERROR, tok, set);
				return ReduceResult.ERROR;
			}
			switch (act.getType())
			{
				case SHIFT:
					
					stateStack.push(act.getTargetIndex());
					
					curNodeStack.push(tok);
					
					tok = null;
					break;
				case REDUCE:
					
					GrammarRule rule = ruleTable[act.getTargetIndex()];
					int state = 0;
					
					NonTerminalNode nt = new NonTerminalNode(rule);
					for (int i=0; i<rule.getTail().size(); ++i)
					{
						state = stateStack.pop();
						nt.addChildFirst(curNodeStack.pop());
					}
					curNodeStack.push(nt);
					state = stateStack.peek();
					stateStack.push(lrTable[state][rule.getHead().getIndex()].getTargetIndex());
					
					return ReduceResult.READY;
					
				case GOTO:
					// do nothing
					break;
				case ACCEPT:
					
					return ReduceResult.ACCEPT;
					
				case NONE:
					
				default: 
					Map<AbstractSymbol, Set<TerminalSymbol>> map = GrammarProcessor.processFirstSets(ruleTable, symbolTable);
					Set<TerminalSymbol> set = new HashSet<TerminalSymbol>();
					for (int i=0; i<lrTable[stateStack.peek()].length; ++i)
						if (lrTable[stateStack.peek()][i] != null && lrTable[stateStack.peek()][i].getType() != LRActionType.NONE) 
							set.addAll(map.get(symbolTable[i]));
					
					error = new ErrorReport(ErrorType.SYNTAX_ERROR, tok, set);
					return ReduceResult.ERROR;
			}
		}	
	}
	// XXX end reduce

	/**
	 * Determines if the input respects the language recognized by this parser
	 * @param s the string to parse
	 * @return true if the input is correctly formed, false otherwise
	 * @throws IOException if an I/O error occurs
	 */
	
	public boolean recognize(String s) throws IOException
	{
		this.reset();
		scanner.setSource(s);
		return recognize();
	}
	
	/**
	 * Determines if the input respects the language recognized by this parser
	 * @param file the file to parse
	 * @return true if the input is correctly formed, false otherwise
	 * @throws FileNotFoundException if the file does not exists
	 * @throws IOException if an I/O error occurs
	 */
	
	public boolean recognize(File file)  throws IOException, FileNotFoundException
	{
		this.reset();
		scanner.setSource(file);
		return recognize();
	}
	
	/**
	 * Determines if the input respects the language recognized by this parser
	 * @param r the reader to parse
	 * @return true if the input is correctly formed, false otherwise
	 * @throws IOException if an I/O error occurs
	 */
	
	public boolean recognize(Reader r) throws IOException
	{
		this.reset();
		scanner.setSource(r);
		return recognize();
	}
	
	/**
	 * Determines if the input respects the language recognized by this parser
	 * @param is the input stream
	 * @return true if the input is correctly formed, false otherwise
	 * @throws IOException if an I/O error occurs
	 */
	
	public boolean recognize(InputStream is) throws IOException
	{
		this.reset();
		scanner.setSource(is);
		return recognize();
	}
	
	/**
	 * Determines if the input respects the language recognized by this parser
	 * @return true if the input is correctly formed, false otherwise
	 * @throws IOException if an I/O error occurs
	 * @throws IllegalStateException  if the source is not ready
	 */
	public boolean recognize() throws IOException, IllegalStateException
	{
		if (!this.scanner.hasSource())
			throw new IllegalStateException();
		
		TerminalNode tok = null;
		while (true)
		{
			if (tok == null)
				tok = this.scanner.scan();
			
			if (tok.getSymbol().getIndex() == SpecialSymbolEnum.ERROR.toInt())
			{
				error = new ErrorReport(ErrorType.LEXICAL_ERROR, tok);
				return false;
			}
			
			LRAction act = lrTable[stateStack.peek()][tok.getSymbol().getIndex()];
			if (act == null)
			{
				Map<AbstractSymbol, Set<TerminalSymbol>> map = GrammarProcessor.processFirstSets(ruleTable, symbolTable);
				Set<TerminalSymbol> set = new HashSet<TerminalSymbol>();
				for (int i=0; i<lrTable[stateStack.peek()].length; ++i)
					if (lrTable[stateStack.peek()][i] != null && lrTable[stateStack.peek()][i].getType() != LRActionType.NONE) 
						set.addAll(map.get(symbolTable[i]));
				
				error = new ErrorReport(ErrorType.SYNTAX_ERROR, tok, set);
				return false;
			}
			switch (act.getType())
			{
				case SHIFT:				
					stateStack.push(act.getTargetIndex());			
					tok = null;
					break;
				case REDUCE:
					GrammarRule rule = ruleTable[act.getTargetIndex()];
					int state = 0;

					for (int i=0; i<rule.getTail().size(); ++i)
						state = stateStack.pop();

					state = stateStack.peek();
					stateStack.push(lrTable[state][rule.getHead().getIndex()].getTargetIndex());
					
					break;
				case GOTO:
					// do nothing
					break;
				case ACCEPT:
					return true;
					
				case NONE:
					
				default: 
					Map<AbstractSymbol, Set<TerminalSymbol>> map = GrammarProcessor.processFirstSets(ruleTable, symbolTable);
					Set<TerminalSymbol> set = new HashSet<TerminalSymbol>();
					for (int i=0; i<lrTable[stateStack.peek()].length; ++i)
						if (lrTable[stateStack.peek()][i] != null && lrTable[stateStack.peek()][i].getType() != LRActionType.NONE) 
							set.addAll(map.get(symbolTable[i]));
					error = new ErrorReport(ErrorType.SYNTAX_ERROR, tok, set);
					return false;
			}
		}	
	}
	
	
	
	
	
	
	
	/**
	 * @return the lrTable
	 */
	LRAction[][] getLrTable()
	{
		return lrTable;
	}

	/**
	 * Returns the grammar recognized by this parser
	 * @return the grammar
	 */
	public GrammarRule[] getGrammar()
	{
		return ruleTable;
	}

	/**
	 * @return the symbolTable
	 */
	
	public AbstractSymbol[] getSymbolTable()
	{
		return symbolTable;
	}

	/**
	 * @return the start symbol
	 */
	
	public NonTerminalSymbol getStartSymbol()
	{
		return (NonTerminalSymbol)symbolTable[startIndex];
	}

	/**
	 * @return the source reader
	 */
	
	public Reader getSource()
	{
		return this.scanner.getSource();
	}


	
	
	public void setCharset(String cs)
	{
		this.scanner.setCharset(cs);
	}
	
	
	public String getCharset()
	{
		return this.scanner.getCharset();
	}


	
	public void setSource(Reader reader)
	{
		this.scanner.setSource(reader);
	}

	
	public void setSource(InputStream is)
	{
		this.scanner.setSource(is);
	}

	
	public void setSource(File file) throws FileNotFoundException
	{
		this.scanner.setSource(file);
	}

	
	public void setSource(String s)
	{
		this.scanner.setSource(s);
	}
	

	
	public String getName()
	{
		return this.scanner.getName();
	}
	
	
	
	@Override
	public void finalize() throws Throwable
	{
		try
		{
			if (lrTable != null)
				for (int i=0; i<lrTable.length; ++i)
					for (int j=0; j<lrTable[i].length; ++j)
						lrTable[i][j] = null;
			
			if (ruleTable != null)
				for (int i=0; i<ruleTable.length; ++i)
					ruleTable[i] = null;
			
			if (symbolTable != null)
				for (int i=0; i<symbolTable.length; ++i)
					symbolTable[i] = null;
			
			if (stateStack != null)
				stateStack.clear();
		}
		catch (Throwable t)
		{
			throw t;
		}
		finally
		{
			super.finalize();
		}
	}

}
