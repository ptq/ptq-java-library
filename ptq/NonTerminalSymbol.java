/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;


/**
 * A grammar non-terminal symbol
 * 
 * @author Alessio Scalici
 */
public class NonTerminalSymbol extends AbstractSymbol
{
	
	/**
	 * Builds a non-terminal symbol
	 * @param name the symbol unique name
	 * @param index the symbol unique index
	 */
	NonTerminalSymbol(String name, int index)
	{
		super(name, index);
	}
	

	/**
	 * @see ptq.AbstractSymbol#toString()
	 */
	@Override
	public String toString()
	{
		return "<"+getName()+">";
	}
	

	
}
