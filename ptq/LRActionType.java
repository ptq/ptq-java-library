/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;


/**
 * An enum type that defines LR action types (SHIFT, REDUCE, GOTO, ACCEPT)
 * 
 * @author Alessio Scalici
 */
enum LRActionType
{
	NONE (0),
	GOTO (1),
	SHIFT (2),
	REDUCE (3),
	ACCEPT (4),
	UNDEFINED(-99);

	private final int code;

	private LRActionType(int code)
	{
		this.code = code;
	}

	/**
	 * Returns the numeric value
	 * @return the numeric value
	 */
	int toInt()
	{
		return code;
	}
	
	/**
	 * Returns the LR action type name
	 * @return the LR action type name
	 */
	String toName()
	{
		return this.toString();
	}
	

	/**
	 * Returns the correct value, given the relative numeric code
	 * @param code the numeric code
	 * @return the value relative to the argument, UNDEFINED if code
	 * doesn't match any value
	 */
	static LRActionType get(int code)
	{
		for (int i = 0; i < values().length; i++)
		{
			if (values()[i].code == code)
			{
				return values()[i];
			}
		}
		return UNDEFINED;
	}
}
