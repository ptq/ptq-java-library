/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;


/**
 * Represents an edge of a scanner DFA, linking
 * two DFA states and labelled with the accepted inputs.
 * 
 * This is a package internal class and cannot be used outside this package
 * 
 * @author Alessio Scalici
 */
class Edge
{
	// the source DFA state
	private BaseScannerState sourceState;
	
	// the target DFA state (this edge points at this state)
	private BaseScannerState targetState;
	
	/*
	 * The inputs accepted by this edge. They are represented this way:
	 * this matrix is Nx2, every row represents a continuous range of integers.
	 * Here's an example:
	 * [0][3]
	 * [12][12]
	 * [18][20]
	 * 
	 * accepts the following inputs:
	 * [0][1][2][3][12][18][19][20]
	 * 
	 * this allows memory saving because edge accepted inputs trend to be contiguous
	 */
	private int[][] ranges;

	/**
	 * Builds an edge
	 * 
	 * @param sourceState the source state
	 * @param targetState the state this edge points to
	 * @param codePointSet a Set containing the inputs accepted by this edge
	 */
	Edge(BaseScannerState sourceState, BaseScannerState targetState, Set<Integer> codePointSet)
	{
		
		this.sourceState = sourceState;
		this.targetState = targetState;
		
		if (codePointSet != null)
			this.ranges = getRanges(codePointSet);
	}
	
	
	/**
	 * Turns an integer Set in ranges representation
	 * @param cpset the integer set
	 * @return the ranges representation of the input set
	 */
	int[][] getRanges(Set<Integer> cpset)
	{
		ArrayList<int[]> res = new ArrayList<int[]>();

		int[] ar = new int[cpset.size()];
		
		int i = 0;
		for (Integer num : cpset)
			ar[i++] = num;
		
		if (ar.length == 0)
			return new int[0][];
		
		int a = ar[0];
		int b = ar[0];
		for (int j=1; j<ar.length; ++j)
		{
			if (ar[j] == b+1)
			{
				b = ar[j];
				continue;
			}
			int[] na = new int[2];
			na[0] = a;
			na[1] = b;
			res.add(na);
			a = ar[j];
			b = ar[j];
		}
		int[] na = new int[2];
		na[0] = a;
		na[1] = b;
		res.add(na);
		
		int[][] res2 = new int[res.size()][];
		for (i=0; i<res.size(); ++i)
			res2[i] = res.get(i);
		
		return res2;
	}
	

	
	/**
	 * @param target
	 */
	void setTarget(BaseScannerState target)
	{
		this.targetState = target;
	}
	
	/**
	 * @return
	 */
	BaseScannerState getTarget()
	{
		return this.targetState;
	}
	
	/**
	 * @param source
	 */
	void setSource(BaseScannerState source)
	{
		this.sourceState = source;
	}
	
	/**
	 * @return
	 */
	BaseScannerState getSource()
	{
		return this.sourceState;
	}
	
	
	
	/**
	 * Returns the accepted input Set
	 * @return the accepted input Set
	 */
	Set<Integer> getInputSet()
	{
		Set<Integer> res = new TreeSet<Integer>();
		for (int[] range : ranges)
			for (int i = range[0]; i<=range[1]; ++i)
				res.add(i);
		return res;
	}
	


	/**
	 * Sets the input Set accepted by this edge
	 * @param inputSet
	 */
	void setInputSet(Set<Integer> inputSet)
	{
		this.ranges = getRanges(inputSet);
	}
	
	/**
	 * Returns true if the integer i is accepted by this edge, false otherwise
	 * 
	 * @param i the input to check
	 * @return true if the integer i is accepted by this edge, false otherwise
	 */
	boolean accepts(int i)
	{	
		int imin = 0;
		int imax = ranges.length-1;
		while (imax >= imin)
	    {
			int imid = (imin + imax) >>> 1; // (imin + imax) / 2
	 
			if (ranges[imid][1] <  i)
				imin = imid + 1;
			else if (ranges[imid][0] > i )
				imax = imid - 1;
			else
				return true;
	    }
		// not found
		return false;
		
	}
	

}
