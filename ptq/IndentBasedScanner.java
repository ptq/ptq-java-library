/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;



import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Stack;
import java.util.TreeMap;



/**
 * A scanner implementation. 
 * 
 * @author Alessio Scalici
 */
public class IndentBasedScanner extends BaseScanner
{
	

	boolean indenting = true;
	int level = 0;
	protected Stack<Integer> levelStack = new Stack<Integer>();
	protected LinkedList<TerminalNode> queue = new LinkedList<TerminalNode>();
	
	public IndentBasedScanner(BaseScanner scan)
	{
		super();
		this.arSymbolTable = scan.arSymbolTable;
		this.firstIgnoredIndex = scan.firstIgnoredIndex;
		this.begCol = scan.begCol;
		this.begLine = scan.begLine;
		this.begPos = scan.begPos;
		this.curCol = scan.curCol;
		this.curLine = scan.curLine;
		this.curPos = scan.curPos;
		this.charsetName = scan.charsetName;
		this.source = scan.source;
		this.streamBufferSize = scan.streamBufferSize;
		
		this.scanners = new TreeMap<String, BaseSubScanner>();
		for (BaseSubScanner oc : scan.scanners.values())
			this.scanners.put(oc.getName(), new IndentBasedSubScanner(oc, this));
		
		for (BaseSubScanner c : this.scanners.values())
			((IndentBasedSubScanner)c).fixActionReferences();
		
		this.initialScanner = this.scanners.get(scan.initialScanner.getName());
		this.currentScanner = scan.currentScanner == null ? null : this.scanners.get(scan.currentScanner.getName());
	}
	
	
	
	
	void processLevel()
	{
		
		// process indentation level before returning the recognized token
		
		if (levelStack.isEmpty())
			levelStack.push(level);
		else
		{
			if (levelStack.peek() < level)
			{
				levelStack.push(level);
				queue.addFirst(createBeginBlock());
			}
			else if (levelStack.peek() > level)
			{
				int prev = levelStack.pop();
				queue.addFirst(createEndBlock());
				
				while (!levelStack.isEmpty() && levelStack.peek() > level)
				{
					prev = levelStack.pop();
					queue.addFirst(createEndBlock());
					if (queue.isEmpty())
						break;
				}
				
				if ((!levelStack.isEmpty() && levelStack.peek() != level) || (levelStack.isEmpty() && prev != level))
						queue.addFirst(createErrorBlock());
			}
		}
		

	}

	
	
	void closePendingBlocks()
	{
		while (levelStack.size() > 1)
		{
			levelStack.pop();
			queue.addFirst(createEndBlock());
		}	
	}
	
	
	
	
	
	


	TerminalNode createBeginBlock()
	{
		TerminalNode res = new TerminalNode(this.arSymbolTable[SpecialSymbolEnum.BEGIN.toInt()]);
		//res.setPosition(new Position(begLine, begCol, begPos));
		res.setData("");
		
		return res;
	}
	
	TerminalNode createEndBlock()
	{
		TerminalNode res = new TerminalNode(this.arSymbolTable[SpecialSymbolEnum.END.toInt()]);
		//res.setPosition(new Position(begLine, begCol, begPos));
		res.setData("");
		return res;
	}

	TerminalNode createErrorBlock()
	{
		TerminalNode res = new TerminalNode(this.arSymbolTable[SpecialSymbolEnum.ERROR.toInt()]);
		//res.setPosition(new Position(begLine, begCol, begPos));
		res.setData("");
		return res;
	}

	

	


	@Override
	public TerminalNode scan() throws IOException
	{
		TerminalNode res = null;

		do
		{
	
			if (queue.isEmpty())
				res = currentScanner.scan();
			
			if (!queue.isEmpty())
			{ 
			
				TerminalNode firstInQueue = queue.pollFirst();
				
				Action act = this.currentScanner.getAction(firstInQueue.getSymbol());
				if (act != null && (act.isEnd() || act.getStart() != null || act.getGoto() != null))
				{
					if (act.isEnd())
						this.end();
					if (act.getStart() != null)
						this.begin(act.getStart());
					if (act.getGoto() != null)
						this.setCurrentScanner(act.getGoto());
					
					if (res != null)// nulls last token
					{
						String data = res.getData();
						for (int i=0; i<data.codePointCount(0, data.length()); ++i)
							source.unread(data.codePointAt(i));
						tokenReset();
					}
				}
				else if (res != null)// put token in queue
				{
					queue.addLast(res);
				}
	
				res = firstInQueue;
			}
			
			
		}
		while (res == null);
		res.setPosition(new Position(begLine, begCol, begPos));
		
			
		return res;
	}




	// -- PACKAGE MEMBERS -- //
	
	protected IndentBasedScanner()
	{
		super();
	}
	
	IndentBasedScanner(Collection<BaseSubScanner> sSet, String initial, TerminalSymbol[] symbols)
	{
		super(sSet, initial, symbols);
	}
	

	public void reset()
	{
		super.reset();
		indenting = true;
		level = 0;
		levelStack.clear();
		queue.clear();
	}
	
	
	@Override
	public void finalize() throws Throwable
	{
		try
		{
			if (levelStack != null)
				levelStack.clear();
			if (queue != null)
				queue.clear();
		}
		catch (Throwable t)
		{
			throw t;
		}
		finally
		{
			super.finalize();
		}
	}
}
