/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;

/**
 * This exception is raised when there's a stream format error 
 * (e.g. the stream is incorrect or corrupted)
 * 
 * @author Alessio Scalici
 */
public class DeserializationException extends Exception
{

	private static final long serialVersionUID = -5954793929484602815L;

	public DeserializationException()
	{
		super();
	}

	public DeserializationException(String arg0, Throwable arg1)
	{
		super(arg0, arg1);
	}

	public DeserializationException(String arg0)
	{
		super(arg0);
	}

	public DeserializationException(Throwable arg0)
	{
		super(arg0);
	}
	
	
}
