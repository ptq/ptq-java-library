/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;

/**
 * An error type enumerator.
 * This indicates the error type in the error report
 * 
 * @author Alessio Scalici
 */
public enum ErrorType
{

	WARNING (0),
	
	/**
	 * A reader error, occurs when the source is not encoded properly or with the correct charset
	 */
	ENCODING_ERROR (1),
	
	/**
	 * An scanner error, it occurs when a token is not recognized
	 */
	LEXICAL_ERROR (2),
	
	/**
	 * An parser error, it occurs when the scanner recognizes an unexpected token
	 */
	SYNTAX_ERROR (3),
	
	/**
	 * An undefined error
	 */
	UNDEFINED(-99);

	private final int code;

	ErrorType(int code)
	{
		this.code = code;
	}

	/**
	 * Returns the numeric value
	 * @return the numeric value
	 */
	public int toInt()
	{
		return code;
	}
	

	/**
	 * Returns the correct value, given the relative numeric code
	 * @param code the numeric code
	 * @return the value relative to the argument, UNDEFINED if code
	 * doesn't match any value
	 */
	public static ErrorType get(int code)
	{
		for (int i = 0; i < values().length; i++)
			if (values()[i].code == code)
				return values()[i];
			
		
		return UNDEFINED;
	}
	

}
