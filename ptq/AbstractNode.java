/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;

/**
 * A generic parse tree node
 * 
 * @author Alessio Scalici
 * @see TerminalNode
 * @see NonTerminalNode
 */
public abstract class AbstractNode
{
	/**
	 * The symbol this node represents
	 */
	protected AbstractSymbol symbol;
	
	/**
	 * The parent node (null if this is the root node)
	 */
	protected NonTerminalNode parent = null;
	
	/**
	 * Construct a new AbstractNode
	 * @param symbol the symbol this node will represent in the parse tree
	 * @throws NullPointerException if symbol is null
	 */
	protected AbstractNode(AbstractSymbol symbol)
	{
		if (symbol == null)
			throw new NullPointerException();
		this.symbol = symbol;
	}

	/**
	 * Returns the symbol this node represents
	 * @return the symbol this node represents
	 */
	public AbstractSymbol getSymbol()
	{
		return symbol;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o)
	{
		if (o == this)
			return true;
		if (o == null || o.getClass() != this.getClass())
			return false;
		
		return this.symbol.equals(((AbstractNode)o).symbol);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return symbol.hashCode();
	}
	
	
	/**
	 * Sets the parent node
	 * @param par the parent node
	 */
	public void setParent(NonTerminalNode par)
	{
		this.parent = par;
	}
	
	/**
	 * Returns the parent node (null if this is the root node)
	 * @return the parent node (null if this is the root node)
	 */
	public NonTerminalNode getParent()
	{
		return this.parent;
	}
	
	
	/**
	 * Returns the position of this node in the source stream
	 * @return the position of this node in the source stream
	 */
	abstract public Position getPosition();
	
	
	/**
	 * Returns the length of this node in the source stream
	 * @return the length of this node in the source stream
	 */
	abstract public int getLength();
}
