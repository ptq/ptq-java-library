/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;

/**
 * The Action class contains the data for handling symbol actions like DFA start, end and switch, newlines
 * and ignoring symbols. 
 * 
 * This class has package visibility only and cannot be used outside this package
 * 
 * @author Alessio Scalici
 *
 */
class Action
{
	// the target scanner for goto/jump action
	private BaseSubScanner gotoTarget;
	
	// ignore this symbol
	private boolean ignore;
	
	// push back this symbol when recognized
	private boolean pushback;
	
	// start a new nested scanner 
	private BaseSubScanner start = null;
	
	// end the internal nested scanner
	private boolean end = false;
	
	// the symbol
	private TerminalSymbol symbol;
	
	// use this token as indentation
	private boolean indent;

	
	/**
	 * Constructs a new action
	 * @param symbol the target symbol
	 * @throws NullPointerException if symbol is null
	 */
	Action(TerminalSymbol symbol)
	{
		if (symbol == null)
			throw new NullPointerException();
		gotoTarget = null;
		this.symbol = symbol;
		this.ignore = false;
	}

	
	/**
	 * Returns the target symbol
	 * @return the target symbol
	 */
	TerminalSymbol getSymbol()
	{
		return symbol;
	}

	
	/**
	 * Sets the target symbol
	 * @param symbol the target symbol
	 */
	void setSymbol(TerminalSymbol symbol)
	{
		this.symbol = symbol;
	}

	/**
	 * Returns the target scanner for goto/jump action
	 * @return the target scanner for goto/jump action
	 */
	BaseSubScanner getGoto()
	{
		return gotoTarget;
	}

	/**
	 * Returns the target scanner for goto/jump action
	 * @param target the target scanner for goto/jump action
	 */
	void setGoto(BaseSubScanner target)
	{
		this.gotoTarget = target;
	}
	
	
	
	/**
	 * Returns true if the scanner should ignore the target symbol, false otherwise
	 * @return true if the scanner should ignore the target symbol, false otherwise
	 */
	boolean isIgnore()
	{
		return ignore;
	}

	
	/**
	 * Tells the scanner it should ignore the target symbol
	 * @param ignore set to true if the scanner should ignore the target symbol, false otherwise
	 */
	void setIgnore(boolean ignore)
	{
		this.ignore = ignore;
	}

	
	/**
	 * Returns the nested scanner that should start when target symbol is recognized
	 * @return the nested scanner that should start when target symbol is recognized
	 */
	BaseSubScanner getStart()
	{
		return start;
	}


	/**
	 * Tells the scanner to start a new nested scanner
	 * @param start the nested scanner to start
	 */
	void setStart(BaseSubScanner start)
	{
		this.start = start;
	}


	/**
	 * Returns true if the scanner should terminate the internal nested scanner, false otherwise
	 * @return true if the scanner should terminate the internal nested scanner, false otherwise
	 */
	boolean isEnd()
	{
		return end;
	}


	/**
	 * Tells the scanner to terminate the internal nested scanner
	 * @param end true if the scanner should terminate the internal nested scanner
	 * when this symbol is recognized, false otherwise
	 */
	void setEnd(boolean endGroup)
	{
		this.end = endGroup;
	}


	/**
	 * @return the pushback
	 */
	public boolean isPushback()
	{
		return pushback;
	}


	/**
	 * @param pushback the pushback to set
	 */
	public void setPushback(boolean pushback)
	{
		this.pushback = pushback;
	}
	
	
	/**
	 * @return indent
	 */
	public boolean isIndent()
	{
		return indent;
	}


	/**
	 * @param indent the indent to set
	 */
	public void setIndent(boolean indent)
	{
		this.indent = indent;
	}
	
}
