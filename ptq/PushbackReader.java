/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;

import java.io.IOException;
import java.io.Reader;
import java.util.Stack;

/**
 * A Reader decorator. Provides methods to un-read characters using a fixed-length buffer.
 * 
 * @author Alessio Scalici
 */
public class PushbackReader extends Reader
{
	
	// the un-read buffer
	private Stack<Integer> stack;
	
	// the input reader
	private Reader reader;
	
	
	/**
	 * Creates a new PushbackReader indicating the buffer size
	 * @param reader the input reader
	 * @throws NullPointerException if reader is null
	 */
	public PushbackReader(Reader reader) throws NullPointerException
	{
		if (reader == null)
			throw new NullPointerException();
		this.reader = reader;
		stack = new Stack<Integer>();
	}

	
	
	/**
	 * Reads a new character from the Reader (or the last un-read character)
	 * @return the next character (or the last un-read character)
	 * @throws IOException if an I/O error occurs
	 */
	public int read() throws IOException
	{
		if (stack.isEmpty())
			return reader.read();
		
		return stack.pop();
	}

	
	/**
	 * Un-reads a character, pushing it into the buffer. It will be re-read calling the
	 * read() method
	 * @param c the character to un-read
	 */
	public void unread(int c)
	{
		stack.push(c);
	}
	
	
	/**
	 * Returns true if a character can be read, false otherwise
	 * @return true if a character can be read, false otherwise
	 * @throws IOException if an I/O error occurs
	 */
	public boolean ready() throws IOException
	{
		if (stack.isEmpty())
			return reader.ready();
		return true;
	}

	/**
	 * Closes the Reader
	 * @throws IOException if an I/O error occurs
	 */
	public void close() throws IOException 
	{
		reader.close();
	}


	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object arg0) 
	{
		if (this == arg0)
			return true;
		if (arg0 == null || !(arg0 instanceof PushbackReader))
			return false;
		
		PushbackReader oo = (PushbackReader)arg0;
		if (!oo.stack.equals(this.stack))
			return false;
		
		return (oo.reader.equals(reader));
	}


	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() 
	{
		return reader.hashCode();
	}

	/**
	 * @param arg0
	 * @throws IOException
	 * @see java.io.Reader#mark(int)
	 */
	public void mark(int arg0) throws IOException 
	{
		reader.mark(arg0);
	}

	/**
	 * @see java.io.Reader#markSupported()
	 */
	public boolean markSupported() 
	{
		return reader.markSupported();
	}
	

	/**
	 * Resets the Reader and clears the buffer
	 * @throws IOException
	 * @see java.io.Reader#reset()
	 */
	public void reset() throws IOException 
	{
		stack.clear(); 
		reader.reset();
	}

	/**
	 * @throws IOException
	 * @see java.io.Reader#skip(long)
	 */
	public long skip(long arg0) throws IOException 
	{
		return reader.skip(arg0);
	}

	
	
	/**
	 * @see java.io.Reader#read(char[])
	 */
	@Override
	public int read(char[] arg0) throws IOException
	{
		return this.read(arg0, 0, arg0.length);
	}
	
	
	/**
	 * @param cbuf
	 * @return the number of bytes read
	 * @throws IOException
	 * @see java.io.Reader#skip(long)
	 */
	@Override
	public int read(char[] cbuf, int off, int len) throws IOException
	{
		if (stack.isEmpty())
			return reader.read(cbuf, off, len);
		
		if (cbuf.length < off + len)
			throw new IndexOutOfBoundsException();
		
		int index = off;
		int res = 0;
		char[] auxC;
		int auxInt;
		while (res < len)
		{
			auxInt = read();
			auxC = Character.toChars(auxInt);
			
			if (index + auxC.length > cbuf.length)
				return res;
			
			for (char c : auxC)
				cbuf[index++] = c;
			++res;
		}
		return res;
	}
	
	
	/**
	 * @see java.lang.Object#finalize()
	 */
	@Override
	public void finalize() throws Throwable
	{
		try
		{
			if (stack != null)
				stack.clear();
		}
		catch (Throwable t)
		{
			throw t;
		}
		finally
		{
			super.finalize();
			this.close();
		}
	}
}
