/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;



/**
 * A scanner implementation. 
 * 
 * @author Alessio Scalici
 */
public class BaseScanner implements Scanner
{
	
	// the scanner name (project name)
	private String name;
	
	// maps dfa name -> dfa
	protected Map<String, BaseSubScanner> scanners;
	
	// ordered array of symbols
	protected TerminalSymbol[] arSymbolTable;
	
	// the current scanner dfa
	protected BaseSubScanner currentScanner;
	
	// the initial scanner dfa
	protected BaseSubScanner initialScanner;
	
	// the source reader
	protected PushbackReader source;
	
	// the initial position index
	public static int INITIAL_LINE = 1;
	public static int INITIAL_COL = 1;
	public static int INITIAL_POS = 0;
	
	// current position data
	protected int curLine;
	protected int curCol;
	protected int curPos;
	
	// position marks
	protected int begLine;
	protected int begCol;
	protected int begPos;
	
	
	// the index of the first symbol ignored by all of the dfas
	protected int firstIgnoredIndex = -1;
	
	// the nested dfa stack
	protected Stack<BaseSubScanner> scannerStack;
	
	// the name of charset used to read the input stream
	protected String charsetName;
	
	// the stream (e.g. file) buffer size
	protected int streamBufferSize;
	
	
	
	
	
	{
		curLine = INITIAL_LINE;
		curCol = INITIAL_COL;
		curPos = INITIAL_POS;
		this.scannerStack = new Stack<BaseSubScanner>();
		charsetName = "us-ascii";
		streamBufferSize = 100;
	}
	

	
	/*
	 * @see ptq3.ScannerInterface#scan()
	 */
	@Override
	public TerminalNode scan() throws IOException
	{
		TerminalNode res = currentScanner.scan();
		while (res == null)
			res = currentScanner.scan();
		res.setPosition(new Position(begLine, begCol, begPos));
	
		return res;
	}
	
	



	public void setSource(Reader s)
	{
		reset();
		this.source = new PushbackReader(s);
	}
	
	public void setSource(InputStream s)
	{
		reset();
		this.source = new PushbackReader(new InputStreamReader(s, Charset.forName(charsetName)));
	}
	
	public void setSource(String s)
	{
		reset();
		this.source = new PushbackReader(new StringReader(s));
	}
	
	

	

	
	public void setSource(InputStream is, Charset cs, int fileBufferSize)
	{
		reset();
		this.source = new PushbackReader(new BufferedReader(new InputStreamReader(is, cs), fileBufferSize));
	}
	
	@Override
	public void setSource(File file) throws FileNotFoundException
	{
		reset();
		this.source = new PushbackReader(new BufferedReader(new InputStreamReader(new FileInputStream(file), Charset.forName(charsetName)), streamBufferSize));
	}
	
	public PushbackReader getSource()
	{
		return source;
	}

	
	/**
	 * Returns a partial symbol table, with just the
	 * terminal symbols that are not ignored
	 * @return a partial symbol table, with just the
	 * terminal symbols that are not ignored
	 */
	public TerminalSymbol[] getPublicSymbolTable()
	{
		TerminalSymbol[] res = new TerminalSymbol[firstIgnoredIndex];
		for (int i=0; i<firstIgnoredIndex; ++i)
			res[i] = arSymbolTable[i];
		return res;
	}

	
	// -- PACKAGE MEMBERS -- //
	
	protected BaseScanner()
	{}
	
	BaseScanner(Collection<BaseSubScanner> sSet, String initial, TerminalSymbol[] symbols)
	{
		
		init(sSet, initial, symbols);
	}
	
	void init(Collection<BaseSubScanner> sSet, String initial, TerminalSymbol[] symbols)
	{
		
		arSymbolTable = symbols;
		
		firstIgnoredIndex = arSymbolTable.length;
		
		
		scanners = new TreeMap<String, BaseSubScanner>();
		for (BaseSubScanner s : sSet)
		{
			scanners.put(s.getName(), s);
			s.setParent(this);
		}
		initialScanner = scanners.get(initial);
		currentScanner = initialScanner;

	}
	
	/**
	 * @return the current scanner
	 */
	BaseSubScanner getCurrentScanner()
	{
		return currentScanner;
	}
	
	/**
	 * Returns the symbol given the name
	 * @param sName the symbol's name
	 * @return the symbol given the name
	 */
	TerminalSymbol getSymbol(String sName)
	{
		for (TerminalSymbol s : this.arSymbolTable)
			if (s.getName().equals(sName))
				return s;
		throw new IllegalArgumentException("'"+sName+"' is not a symbol name");
	}
	
	/**
	 * Returns the symbol given the index
	 * @param index the symbol index
	 * @return the symbol given the index
	 */
	TerminalSymbol getSymbol(int index)
	{
		return this.arSymbolTable[index];
	}
	
	
	/**
	 * Starts a new nested DFA
	 * @param g the new nested DFA
	 */
	void begin(BaseSubScanner g)
	{
		if (g != null)
		{
			scannerStack.push(this.currentScanner);
			this.currentScanner = g;
		}
	}
	
	/**
	 * Terminates the current nested DFA
	 */
	void end()
	{
		if (scannerStack.isEmpty())
			return;
		
		this.currentScanner = scannerStack.pop();
	}
	
	/**
	 * Changes the current dfa scanner
	 * @param newScanner the dfa scanner to use
	 */
	void setCurrentScanner(BaseSubScanner newScanner)
	{
		this.currentScanner = newScanner;
	}
	
	/**
	 * Marks the token begin position
	 */
	void tokenBegin()
	{
		begLine = curLine;
		begCol = curCol;
		begPos = curPos;
	}
	

	
	/**
	 * Resets the current position
	 */
	void tokenReset()
	{
		curLine = begLine;
		curCol = begCol;
		curPos = begPos;
	}
	
	
	

	
	/**
	 * Increase the current position
	 */
	void newCol(int amt)
	{
		curCol += amt;
		curPos += amt;
	}
	
	/**
	 * @return the scanner dfa collection
	 */
	Collection<BaseSubScanner> getScanners()
	{
		return this.scanners.values();
	}
	
	/**
	 * @return the initial scanner dfa
	 */
	BaseSubScanner getInitialScanner()
	{
		return this.initialScanner;
	}

	
	/**
	 * After a skip/ignore based sort of the symbol table, sets the first ignored symbol index
	 * @param i the index of the first symbol ignored by all of the dfas
	 */
	void setFirstIgnoredIndex(int i)
	{
		this.firstIgnoredIndex = i;
	}

	/**
	 * Returns the entire symbol table
	 * @return the entire symbol table
	 */
	public TerminalSymbol[] getInternalSymbolTable()
	{
		return this.arSymbolTable;
	}

	/** 
	 * Resets the scanner and nulls the source reader
	 * @see ptq.Scanner#reset()
	 */
	@Override
	public void reset()
	{
		curLine = INITIAL_LINE;
		curCol = INITIAL_COL;
		curPos = INITIAL_POS;
		begLine = INITIAL_LINE;
		begCol = INITIAL_COL;
		begPos = INITIAL_POS;
		this.scannerStack.clear();
		source = null;
		this.currentScanner = initialScanner;
	}

	/**
	 * Returns true if the scanner has a source stream, false otherwise
	 * @return true if the scanner has a source stream, false otherwise
	 * @see ptq.Scanner#hasSource()
	 */
	@Override
	public boolean hasSource()
	{
		return (source != null);
	}

	/**
	 * Returns the name of the charset used to read from source stream
	 * @return the name of the charset used to read from source stream
	 */
	public String getCharset()
	{
		return charsetName;
	}

	/**
	 * Sets the name of the charset used to read from source stream
	 * @param charsetName the name of the charset used to read from source stream
	 */
	public void setCharset(String charsetName)
	{
		this.charsetName = charsetName;
	}

	/**
	 * Returns the size of the stream reader buffer.
	 * @return the streamBufferSize the size of the stream reader buffer.
	 */
	public int getStreamBufferSize()
	{
		return streamBufferSize;
	}

	/**
	 * Sets the size of the stream reader buffer. Optimizes performances when reading from
	 * a file or other performance-expensive sources.
	 * @param streamBufferSize the size of the stream reader buffer. Optimizes performances when reading from
	 * a file or other performance-expensive sources.
	 */
	public void setStreamBufferSize(int streamBufferSize)
	{
		this.streamBufferSize = streamBufferSize;
	}



	/**
	 * Returns the subscanner with the spacified name
	 * @param name the subscanner name
	 * @return the subscanner with the spacified name
	 */
	public BaseSubScanner getSubScanner(String name)
	{
		return scanners.get(name);
	}
	
	
	/**
	 * Returns the name of this scanner (the project name)
	 * @return the name of this scanner (the project name)
	 */
	public String getName()
	{
		return this.name;
	}
	
	
	
	
	@Override
	public void finalize() throws Throwable
	{
		
		try
		{
			reset();
			
			if (arSymbolTable != null)
			{
				for (int i=0; i<arSymbolTable.length; ++i)
					arSymbolTable[i] = null;
				arSymbolTable = null;	
			}
			
			if (currentScanner != null)
				currentScanner = null;
			
			if (initialScanner != null)
				initialScanner = null;
			
			if (scanners != null)
			{
				scanners.clear();
				scanners = null;
			}
			
			if (source != null)
			{
				source.close();
				source = null;
			}
			
			if (scannerStack != null)
			{
				scannerStack.clear();
				scannerStack = null;
			}
		}
		catch (Throwable t)
		{
			throw t;
		}
		finally
		{
			super.finalize();
		}
		
	}


	/**
	 * Sets the scanner's name (project's name)
	 */
	void setName(String name) 
	{
		this.name = name;
	}
	
	
}
