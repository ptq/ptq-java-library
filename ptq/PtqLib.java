/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;

public interface PtqLib
{
	public static int MAJOR_VERSION = 1;
	public static int MINOR_VERSION = 0;
	public static int PATCH_VERSION = 0;

}
