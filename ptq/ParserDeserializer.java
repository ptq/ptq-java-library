/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * Provides methods to deserialize a parser
 * 
 * @author Alessio Scalici
 */
public class ParserDeserializer extends BaseDeserializer
{
	
	static private int parserType;
	static BaseScanner scanner;
	
	/**
	 * Returns the parser deserialized from the input stream
	 * @param is the scanner input stream
	 * @return  the parser deserialized from the input stream
	 * @throws IOException if an I/O error occurs
	 * @throws VersionNotSupportedException if the parser requires a greater Ptq library version
	 * @throws DeserializationException if the file is corrupted, or is not a Ptq file
	 */
	public static Parser deserialize(InputStream is) throws IOException, VersionNotSupportedException, DeserializationException
	{
		ParserDeserializer.inputStream = is;
		
		readHeader();
		readInfoSection();
		
		scanner = ScannerDeserializer.deserializeSection(is);
		
		scanner.setName(name);
		
		return readParserSection();
	}
	
	/**
	 * Reads the parser section of the stream
	 * @return the deserialized parser
	 * @throws IOException if an I/O error occurs
	 * @throws DeserializationException if the file is corrupted, or is not a Ptq file
	 */
	static private Parser readParserSection() throws IOException, DeserializationException
	{
		int c = readHTF8();
		if (c != PtqFileConst.BEGIN_PARSER_SECTION)
			throw new DeserializationException("Error reading Parser section begin (expected "+PtqFileConst.BEGIN_PARSER_SECTION+", read "+c+")");

		
		parserType = readHTF8();
			
		
		c = readHTF8();
		if (c != PtqFileConst.BYTE_RECORD_SEPARATOR)
			throw new DeserializationException("Error reading Parser section end (expected "+PtqFileConst.BYTE_RECORD_SEPARATOR+", read "+c+")");

		AbstractSymbol[] symbolTable = readSymbolTableSection();
		
		int startSymbol = readHTF8();
		c = readHTF8();
		if (c != PtqFileConst.BYTE_RECORD_SEPARATOR)
			throw new DeserializationException("Error reading Parser section end (expected "+PtqFileConst.BYTE_RECORD_SEPARATOR+", read "+c+")");

		GrammarRule[] ruleTable = readRuleTableSection(symbolTable);
		
		

		switch (parserType)
		{
			case PtqFileConst.PARSER_TYPE_LR0:
			case PtqFileConst.PARSER_TYPE_SLR:
			case PtqFileConst.PARSER_TYPE_LR1:
			case PtqFileConst.PARSER_TYPE_LALR1:
			case PtqFileConst.PARSER_TYPE_LRED1:
				LRAction[][] lrTable = readLR1TableSection();
//				scanner = ScannerDeserializer.deserializeSection(is);

				return new Parser(lrTable, ruleTable, symbolTable, startSymbol, scanner);

			default:
				throw new DeserializationException("Unknown parser type");

		}
		
	}

	


	/**
	 * Reads the symbol table section
	 * @return the symbol table
	 * @throws IOException if an I/O error occurs
	 * @throws DeserializationException if the file is corrupted, or is not a Ptq file
	 */
	static private AbstractSymbol[] readSymbolTableSection() throws IOException, DeserializationException
	{
		
		
		int c = readHTF8();
		if (c != PtqFileConst.BEGIN_SYMBOL_SECTION)
			throw new DeserializationException("Error reading Symbol Table section begin (expected "+PtqFileConst.BEGIN_SYMBOL_SECTION+", read "+c+")");

		int iSymbolCount = readHTF8();
		
		c = readHTF8();
		if (c != PtqFileConst.BYTE_UNIT_SEPARATOR)
			throw new DeserializationException("Error reading Symbol Table section Unit Separator (expected "+PtqFileConst.BYTE_UNIT_SEPARATOR+", read "+c+")");

		int iFstNonTerminalIndex = readHTF8();
		
		TerminalSymbol[] scannerSymbolTable = scanner.getPublicSymbolTable();
		AbstractSymbol[] symbolTable = new AbstractSymbol[iSymbolCount];
		for (int i=0; i<iFstNonTerminalIndex; ++i)
			symbolTable[i] = scannerSymbolTable[i];
		
		for (int i=iFstNonTerminalIndex; i<iSymbolCount; ++i)
			symbolTable[i] = new NonTerminalSymbol(readString(), i);
		
		c = readHTF8();
		if (c != PtqFileConst.BYTE_RECORD_SEPARATOR)
			throw new DeserializationException("Error reading Symbol Table section end (expected "+PtqFileConst.BYTE_RECORD_SEPARATOR+", read "+c+")");

		return symbolTable;
	}
	
	/**
	 * Reads the rule table section
	 * @param symbolTable the previously read symbol table
	 * @return  the rule table
	 * @throws IOException if an I/O error occurs
	 * @throws DeserializationException if the file is corrupted, or is not a Ptq file 
	 */
	static private GrammarRule[] readRuleTableSection(AbstractSymbol[] symbolTable) throws IOException, DeserializationException
	{
		int c = readHTF8();
		if (c != PtqFileConst.BEGIN_RULE_SECTION)
			throw new DeserializationException("Error reading Rule Table section begin (expected "+PtqFileConst.BEGIN_RULE_SECTION+", read "+c+")");

		int iRuleCount = readHTF8();
		
		c = readHTF8();
		if (c != PtqFileConst.BYTE_UNIT_SEPARATOR)
			throw new DeserializationException("Error reading Rule Table section Unit Separator (expected "+PtqFileConst.BYTE_UNIT_SEPARATOR+", read "+c+")");

		GrammarRule[] ruleTable = new GrammarRule[iRuleCount];
		
		for (int i=0; i<iRuleCount; ++i)
		{
			int iRuleLength = readHTF8();
			List<AbstractSymbol> tail = new LinkedList<AbstractSymbol>();
			
			int iHeadIndex = readHTF8();
			if (iHeadIndex >= symbolTable.length)
				throw new DeserializationException("The stream contains corrupt data (index out of bounds)");
			
			if (!(symbolTable[iHeadIndex] instanceof NonTerminalSymbol))
				throw new DeserializationException("The stream contains corrupt data (head of rule is terminal)");
			
			for (int j=1; j<iRuleLength; ++j)
			{
				int iSymbolIndex = readHTF8();
				if (iHeadIndex >= symbolTable.length)
					throw new DeserializationException("The stream contains corrupt data (index out of bounds)");
				tail.add(symbolTable[iSymbolIndex]);
			}
			ruleTable[i] = new GrammarRule(i, (NonTerminalSymbol)symbolTable[iHeadIndex], tail);
		}
		
		if ((c = readHTF8()) != PtqFileConst.BYTE_RECORD_SEPARATOR)
			throw new DeserializationException("Error reading Rule Table section (expected "+PtqFileConst.BYTE_RECORD_SEPARATOR+", read "+c+")");

		return ruleTable;
	}
	
	/**
	 * Reads the LR action-goto table section
	 * @return the LR action-goto table
	 * @throws IOException if an I/O error occurs
	 * @throws DeserializationException  if the file is corrupted, or is not a Ptq file 
	 */
	static private LRAction[][] readLR1TableSection() throws IOException, DeserializationException
	{
		int c = readHTF8();
		if (c != PtqFileConst.BEGIN_TABLE_SECTION)
			throw new DeserializationException("Error reading LR1 section begin (expected "+PtqFileConst.BEGIN_TABLE_SECTION+", read "+c+")");

		int iRows = readHTF8();
		c = readHTF8();
		if (c != PtqFileConst.BYTE_UNIT_SEPARATOR)
			throw new DeserializationException("Error reading LR1 section Unit Separator (expected "+PtqFileConst.BYTE_UNIT_SEPARATOR+", read "+c+")");

		int iCols = readHTF8();
		c = readHTF8();
		if (c != PtqFileConst.BYTE_UNIT_SEPARATOR)
			throw new DeserializationException("Error reading LR1 section Unit Separator (expected "+PtqFileConst.BYTE_UNIT_SEPARATOR+", read "+c+")");

		int iActionCount = readHTF8();
		c = readHTF8();
		if (c != PtqFileConst.BYTE_UNIT_SEPARATOR)
			throw new DeserializationException("Error reading LR1 section Unit Separator (expected "+PtqFileConst.BYTE_UNIT_SEPARATOR+", read "+c+")");

		LRAction[][] lrTable = new LRAction[iRows][iCols];
		for (int i=0; i<iActionCount; ++i)
		{
			int iRow = readHTF8();
			int iCol = readHTF8();
			LRActionType action = LRActionType.get(readHTF8());
				
			switch (action)
			{
				case NONE:
					break;
				case ACCEPT:
					lrTable[iRow][iCol] = new LRAction(action);
					break;
				case SHIFT:
				case GOTO:
				case REDUCE:
					int iTarget = readHTF8();
					lrTable[iRow][iCol] = new LRAction(action, iTarget);
					break;
				default:
					throw new DeserializationException("Error reading LR1 section: undefined LRAction");
			}
				
		}
		
		if ((c = readHTF8()) != PtqFileConst.BYTE_RECORD_SEPARATOR)
			throw new DeserializationException("Error reading LR1 Table section (expected "+PtqFileConst.BYTE_RECORD_SEPARATOR+", read "+c+")");

		return lrTable;
	}
	
	
}
