/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;

import java.io.ByteArrayOutputStream;
import java.io.IOException;


/**
 * Provides facility methods to serialize objects
 * 
 * @author Alessio Scalici
 * @see java.io.ByteArrayOutputStream
 */
class ByteArrayWriter extends ByteArrayOutputStream
{
	
	/**
	 * Writes a string to the stream using the given charset
	 * @param st the string to write
	 * @param charset the name of the charset
	 * @throws IOException
	 * @throws UnsupportedCharsetException if the charset is not supported on this JVM
	 */
	void write(String st, String charset) throws IOException
	{
		byte[] bb = st.getBytes(charset);
		writeHTF8(bb.length);
		this.write(bb);
	}
	
	/**
	 * Writes a boolean to the stream (1 = true, 0 = false)
	 * @param b the boolean to write
	 * @throws IOException
	 */
	void write(boolean b) throws IOException
	{
		if (b)
			this.write((byte)1);
		else
			this.write((byte)0);
	}	

	
	/**
	 * Writes a positive integer in HTF8 format to the stream
	 * @param i the integer to write
	 * @throws IOException
	 * @throws IllegalArgumentException if (i < 0 || i >= 0x80000000)
	 */
	void writeHTF8(int i) throws IOException
	{
		if (i < 0)
			throw new IllegalArgumentException(i+" < 0");
		byte[] bb = null;
		if (i < 0x80)
		{
			bb = new byte[1];
			bb[0] = (byte)(i & 0xFF);
		}
		else if (i < 0x800)
		{
			bb = new byte[2];
			bb[0] = (byte)((i >>> 6) & 0x1F |	0xC0);
			bb[1] = (byte)((i      ) & 0x3F	|	0x80);
		}
		else if (i < 0x10000)
		{
			bb = new byte[3];
			bb[0] = (byte)((i >> 12) & 0xF  |	0xE0);
			bb[1] = (byte)((i >> 6 ) & 0x3F	|	0x80);
			bb[2] = (byte)((i      ) & 0x3F	|	0x80);
		}
		else if (i < 0x200000)
		{
			bb = new byte[4];
			bb[0] = (byte)((i >> 18) & 0x7  |	0xF0);
			bb[1] = (byte)((i >> 12) & 0x3F	|	0x80);
			bb[2] = (byte)((i >> 6 ) & 0x3F	|	0x80);
			bb[3] = (byte)((i      ) & 0x3F	|	0x80);
		}
		else if (i < 0x4000000)
		{
			bb = new byte[5];
			bb[0] = (byte)((i >> 24) & 0x3  |	0xF8);
			bb[1] = (byte)((i >> 18) & 0x3F |	0x80);
			bb[2] = (byte)((i >> 12) & 0x3F	|	0x80);
			bb[3] = (byte)((i >> 6 ) & 0x3F	|	0x80);
			bb[4] = (byte)((i      ) & 0x3F	|	0x80);
		}
		else// if (i <= 0x7FFFFFFF) // Integer.MAX_VALUE
		{
			bb = new byte[6];
			bb[0] = (byte)((i >> 30) & 0x1  |	0xFC);
			bb[1] = (byte)((i >> 24) & 0x3F |	0x80);
			bb[2] = (byte)((i >> 18) & 0x3F |	0x80);
			bb[3] = (byte)((i >> 12) & 0x3F	|	0x80);
			bb[4] = (byte)((i >> 6 ) & 0x3F	|	0x80);
			bb[5] = (byte)((i      ) & 0x3F	|	0x80);
		}
		
		this.write(bb);
	}

}
