/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;

/**
 * A grammar symbol
 * 
 * @author Alessio Scalici
 * @see ptq.TerminalSymbol
 * @see ptq.NonTerminalSymbol
 */
abstract public class AbstractSymbol
{

	// the symbol name as defined in the grammar
	protected String name;
	
	// the symbol index uniquely identifies the symbol into a scanner/parser
	protected int index;

	
	
	/**
	 * Constructs a symbol with the given name and index
	 * @param name the symbol name
	 * @param index the symbol index
	 * @throws NullPointerException if name is null
	 * @throws IllegalArgumentException if index < 0
	 */
	AbstractSymbol(String name, int index) throws NullPointerException, IllegalArgumentException
	{
		if (name == null)
			throw new NullPointerException();
		if (index < 0)
			throw new IllegalArgumentException();
		this.name = name;
		this.index = index;
	}
	

	
	/**
	 * Returns the symbol name
	 * @return the symbol name
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Returns the symbol index
	 * @return the symbol index
	 */
	public int getIndex()
	{
		return index;
	}
	

	@Override
	public String toString()
	{
		return this.name;
	}
	
	
	/**
	 * Every symbol has unique name, so symbols with the same name are the same symbol
	 */
	@Override
	public boolean equals(Object o)
	{
		if (o == this)
			return true;
		if (o == null || this.getClass() != o.getClass())
			return false;

		return this.name.equals(((AbstractSymbol)o).name);
	}

	

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return name.hashCode();
	}

	
	/**
	 * Sets the symbol index
	 * @param index the index to set
	 */
	void setIndex(int index)
	{
		this.index = index;
	}

}
