/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;


import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

/**
 * Contains the scanning algorythm and represents a single
 * scanner DFA. It also handles symbol actions like DFA start, end and switch, newlines
 * and ignoring symbols. 
 * 
 * 
 * @author Alessio Scalici
 *
 */
class BaseSubScanner
{
	

	// the DFA name
	protected String name;
	
	// the DFA parent scanner
	protected BaseScanner parent = null;

	// the DFA initial state
	protected BaseScannerState initialState;
	
	// maps terminal symbol -> action
	protected Map<TerminalSymbol, Action> actions;
	
	// buffer used when scanning
	protected LinkedList<Integer> buf = new LinkedList<Integer>();
	
	
	{
		this.actions = new HashMap<TerminalSymbol, Action>();
	}
	
	
	/**
	 * Builds a scanner DFA
	 * @param sName the name of DFA in the scanner specification
	 * @param oRootState the DFA initial state
	 */
	BaseSubScanner(String sName, BaseScannerState oRootState)
	{
		super();
		this.name = sName;
		this.initialState = oRootState;
	}
	
	
	
	
	/**
	 * Sets the parent scanner for this DFA
	 * @param parent the parent scanner for this DFA
	 */
	void setParent(BaseScanner parent)
	{
		this.parent = parent;
	}
	
	/**
	 * Returns the action object associated with the specified symbol, if it
	 * doesn't exist, it will be created before returning it
	 * @param sym the teminal symbol
	 * @return the action object associated with the specified symbol
	 */
	Action upsertAction(TerminalSymbol sym)
	{
		Action act;
		if (actions.containsKey(sym))
		{
			act = actions.get(sym);
		}
		else
		{
			act = new Action(sym);
			actions.put(sym, act);
		}
		return act;
	}
	
	
	/**
	 * Returns the Action object if this sub-scanner executes actions at the given symbol recognition,
	 * null otherwise
	 * @param sym the symbol
	 * @return the Action object if this sub-scanner executes actions at the given symbol recognition,
	 * null otherwise
	 */
	Action getAction(TerminalSymbol sym)
	{
		if (actions.containsKey(sym))
			return actions.get(sym);
		return null;
	}
	
	/**
	 * Sets an action for this sub-scanner. The related object is contained in the Action object
	 * passed as argument
	 * @param a the Action object
	 */
	void setAction(Action a)
	{
		actions.put(a.getSymbol(), a);
	}
	
	

	/**
	 * @return the name of this DFA
	 */
	String getName()
	{
		return name;
	}
	
	
	/**
	 * Returns the next state given the current state and an input.
	 * If no state can be reached for this input, returns null
	 * @param curState the current DFA state
	 * @param cp the input code point
	 * @return the next state if the input is accepted, null otherwise
	 */
	protected BaseScannerState nextState(BaseScannerState curState, Integer cp)
	{
		
		for (Edge ed : curState.getEdges())
		{
			if (ed.accepts(cp))
			{	
				return ed.getTarget();
			}
		}
		return null;
	}

	
	
	/**
	 * Recognizes the next token. If the recognized token has to be ignored
	 * returns null, otherwise returns the token node. If a lexical error occurs, returns
	 * the special error token.
	 * @return the next token, null if the token has to be ignored, the special error token
	 * if a lexical error occurs
	 * @throws IOException if an I/O error occurs
	 */
	public TerminalNode scan() throws IOException
	{		
		if (parent == null)
			throw new IllegalStateException("parent cannot be null");
		
		TerminalNode res = null;

		int dataIndex = 0;

		BaseScannerState curState = initialState;

		TerminalSymbol acceptedSymbol = this.parent.getSymbol(SpecialSymbolEnum.ERROR.toInt());

		parent.tokenBegin();
		

		// read the next character
		int prevChar = -1;
		int curChar = parent.getSource().read();

		
		// check if there's no more characters in the stream
		if (curChar < 0)
		{
			res = new TerminalNode(parent.getSymbol(SpecialSymbolEnum.EOS.toInt()));
			res.setData("");
			return res;
		}

		while (curChar >= 0)
		{
			
			buf.add(curChar);
			BaseScannerState next = nextState(curState, curChar);
			if (next == null)
			{
				break; // exit loop
			}
			else
			{
				curState = next;
				if (curState.isFinal())
				{
					// update token data
					parent.newCol(buf.size() - dataIndex);
					dataIndex = buf.size();
					acceptedSymbol = curState.getAcceptedSymbol();
				}
			}	

			// unicode newline
			if (curChar == 0xA || curChar == 0xD || curChar == 0xC || curChar == 0xB || curChar == 0x85 || curChar == 0x2028 || curChar == 0x2029)
			{
				if (!(prevChar == 0xD && curChar == 0xA))
					++parent.curLine;
				parent.curCol = BaseScanner.INITIAL_COL;
			}
			
			prevChar = curChar;
			curChar = parent.getSource().read();
			
		}// end for
		
		
		// retrieve action for this symbol
		Action act;
		if (actions.containsKey(acceptedSymbol))
			act = actions.get(acceptedSymbol);
		else
			act = null;
		
		
		// action handling
		if(act != null)
		{ 
			if (act.isPushback() && (act.getGoto() != null || act.getStart() != null || act.isEnd()))
			{
				while (!buf.isEmpty())
					parent.getSource().unread(buf.removeLast());
				parent.tokenReset();
				res = null; // ignore token
			}
			else
				res = new TerminalNode(acceptedSymbol);
			
			if (act.isEnd())
				parent.end();
			if (act.getStart() != null)
				parent.begin(act.getStart());
			if (act.getGoto() != null)
				parent.setCurrentScanner(act.getGoto());	
		}
		else
			res = new TerminalNode(acceptedSymbol);
		
		
		if (res != null)
		{
			if (acceptedSymbol.getIndex() == SpecialSymbolEnum.ERROR.toInt())
			{
				// unread the buffer data
				while(!buf.isEmpty())
					parent.getSource().unread(buf.removeLast());
				parent.tokenReset();
		
				parent.newCol(1);
				res.setData(String.copyValueOf(Character.toChars(parent.getSource().read())));
			}
			else // non errore
			{
				int n = buf.size()-dataIndex;
				for (int i=0; i<n; ++i)
					parent.getSource().unread(buf.removeLast());
				
				res = new TerminalNode(acceptedSymbol);
				
				StringBuffer sbuf = new StringBuffer();
				while (!buf.isEmpty())
					sbuf.append(Character.toChars(buf.removeFirst()));
				res.setData(sbuf.toString());

			}
		}
		
		buf.clear();
		
		if (act != null && act.isIgnore())
			return null; // ignore token
		
		return res;
	}
	
	
	
	
	/**
	 * @return the DFA initial state
	 */
	BaseScannerState getInitialState()
	{
		return this.initialState;
	}
	
	
	/**
	 * Returns true if the symbol can be returned by this DFA, false otherwise
	 * @param term the symbol to check
	 * @return true if the symbol can be returned by this DFA, false otherwise
	 */
	boolean hasTerminal(TerminalSymbol term)
	{
		Set<BaseScannerState> res = getStates();
		for (BaseScannerState s : res)
			if (s.isFinal() && s.getAcceptedSymbol().equals(term))
				return true;
		
		return false;
	}
	
	
	/**
	 * Returns the set of states
	 * @return the set of states
	 */
	Set<BaseScannerState> getStates()
	{
		Set<BaseScannerState> res = new HashSet<BaseScannerState>();
		Stack<BaseScannerState> stack = new Stack<BaseScannerState>();
		res.add(initialState);
		stack.push(initialState);
		while (!stack.isEmpty())
		{
			BaseScannerState curState = stack.pop();
			for (Edge edge : curState.getEdges())
				if (!res.contains(edge.getTarget()))
				{
					res.add(edge.getTarget());
					stack.add(edge.getTarget());
				}
		}
		return res;
	}
	
	/**
	 * Returns the set of edges between states
	 * @return the set of edges between states
	 */
	Set<Edge> getEdges()
	{
		Set<Edge> res = new HashSet<Edge>();
		for (BaseScannerState s : getStates())
			for (Edge e : s.getEdges())
				res.add(e);
		return res;
	}
	
	/**
	 * Returns the DFA action map
	 * @return the DFA action map
	 */
	Map<TerminalSymbol, Action> getActions()
	{
		return actions;
	}
	
	@Override
	public void finalize() throws Throwable
	{
		try
		{
			if (actions != null)
				actions.clear();
			if (buf != null)
				buf.clear();
		}
		catch (Throwable t)
		{
			throw t;
		}
		finally
		{
			super.finalize();
		}
	}

	
}
