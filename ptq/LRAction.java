/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;

/**
 * An LR parser action. Tells the parser to shift to the next token, reduce a rule, goto a state
 * or to accept the input.
 * 
 * This is a package internal class
 * 
 * @author Alessio Scalici
 */
class LRAction
{
	// the action to take (SHIFT,REDUCE,GOTO,ACCEPT,NONE)
	private LRActionType type;
	
	// the target index (GOTO -> the target state index, REDUCE -> the target rule index, SHIFT -> the target state index)
	private int targetIndex;
	
	/**
	 * Builds an LR action
	 * @param type the action type
	 */
	LRAction(LRActionType type)
	{
		super();
		this.type = type;
		targetIndex = -1;
	}

	
	/**
	 * Builds an LR action
	 * @param type the action type
	 * @param targetIndex the action target index:<br/>
	 * - GOTO   -> the target state index<br/>
	 * - REDUCE -> the target rule index<br/>
	 * - SHIFT  -> the target state index<br/>
	 */
	LRAction(LRActionType type, int targetIndex)
	{
		super();
		this.type = type;
		this.targetIndex = targetIndex;
	}

	
	/**
	 * @return the LR action type
	 */
	LRActionType getType()
	{
		return type;
	}

	/**
	 * Sets the LR action type
	 * @param type the LR action type
	 */
	void setType(LRActionType type)
	{
		this.type = type;
	}

	
	/**
	 * @return the LR action target index
	 */
	int getTargetIndex()
	{
		return targetIndex;
	}

	
	/**
	 * Sets the target index
	 * @param targetIndex the LR action target index
	 */
	void setTargetIndex(int targetIndex)
	{
		this.targetIndex = targetIndex;
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		if (targetIndex >= 0)
			return type.toString()+" "+this.targetIndex;
		else
			return type.toString();
	}
	
}
