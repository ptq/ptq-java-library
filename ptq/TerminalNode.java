/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;

/**
 * A terminal parse tree node.
 * 
 * @author Alessio Scalici
 */
public class TerminalNode extends AbstractNode
{
	// the recognized token data
	private String data;
	
	// the token position
	private Position pos; 
	
	{
		data = "";
		pos = new Position();
	}
	
	
	/**
	 * Builds a terminal node
	 * @param oSymbol the terminal symbol this node represents
	 */
	public TerminalNode(TerminalSymbol oSymbol)
	{
		super(oSymbol);
	}
	
	/* (non-Javadoc)
	 * @see ptq3.AbstractNode#getSymbol()
	 */
	@Override
	public TerminalSymbol getSymbol()
	{
		return (TerminalSymbol)symbol;
	}


	/**
	 * Returns the recognized string
	 * @return the recognized string
	 */
	public String getData()
	{
		return data;
	}
	
	/**
	 * @return true if this is the end-of-stream token
	 */
	public boolean isEOS()
	{
		return this.symbol.getIndex() == SpecialSymbolEnum.EOS.toInt();
	}
	
	/**
	 * @return true if this is an error token
	 */
	public boolean isError()
	{
		return this.symbol.getIndex() == SpecialSymbolEnum.ERROR.toInt();
	}
	
	/* (non-Javadoc)
	 * @see ptq3.AbstractNode#getPosition()
	 */
	@Override
	public Position getPosition()
	{
		return pos;
	}
	
	/* (non-Javadoc)
	 * @see ptq3.AbstractNode#getLength()
	 */
	@Override
	public int getLength()
	{
		
		return (data == null || isEOS() ? 0 : data.length());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return this.symbol+": "+this.data+" ("+this.pos.getLine()+","+this.pos.getCol()+") pos: "+this.pos.getIndex();
	}
	
	/* (non-Javadoc)
	 * @see ptq3.AbstractNode#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o)
	{
		if (!super.equals(o))
			return false;
		TerminalNode oo = (TerminalNode)o;
		
		return (
				(data == null ? oo.data == null : this.data.equals(oo.data)) &&
				(pos == null ? oo.pos == null : this.pos.equals(oo.pos))
				);
	}
	
	/* (non-Javadoc)
	 * @see ptq3.AbstractNode#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return super.hashCode() ^ (data == null ? 0 : data.hashCode() ) ^ (pos == null ? 0 : pos.hashCode() );
	}

	
	/**
	 * Sets the symbol this node represents
	 * @param oSymbol the symbol this node represents
	 */
	public void setSymbol(TerminalSymbol oSymbol)
	{
		symbol = oSymbol;
	}

	
	/**
	 * Sets the recognized string
	 * @param data the recognized string
	 */
	public void setData(String data)
	{
		this.data = data;
	}
	
	/**
	 * Sets the position
	 * @param pos the position
	 */
	public void setPosition(Position pos)
	{
		this.pos = pos;
	}


}
