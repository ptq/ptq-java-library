/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;


/**
 * Defines a generic scanner
 * 
 * @author Alessio Scalici
 */
public interface Scanner 
{
	
	
	/**
	 * Sets the input source charset
	 * @param cs the source charset
	 */
	public void setCharset(String cs);
	
	/**
	 * Returns the input source charset
	 * @return the source charset name
	 */
	public String getCharset();
	
	/**
	 * Sets the input source
	 * @param reader the source Reader
	 */
	public void setSource(Reader reader);
	
	/**
	 * Sets the input source
	 * @param is the source input stream
	 */
	public void setSource(InputStream is);
	
	/**
	 * Sets the input source
	 * @param file the source file
	 * @throws FileNotFoundException 
	 */
	public void setSource(File file) throws FileNotFoundException;
	
	/**
	 * Sets the input source
	 * @param s the source String
	 */
	public void setSource(String s);
	
	
	/**
	 * Returns true if the scanner has a source stream, false otherwise
	 * @return true if the scanner has a source stream, false otherwise
	 */
	public boolean hasSource();
	
	/**
	 * Resets the scanner and nulls the source reader
	 * @throws IOException if an I/O error occurs
	 */
	public void reset() throws IOException;
	
	/**
	 * Returns the next recognized token, or the special error token if a lexical error occurs
	 * @return the next recognized token, or the special error token if a lexical error occurs
	 * @throws IOException if an I/O error occurs
	 */
	public TerminalNode scan() throws IOException;

	/**
	 * Returns the source input stream
	 * @return the source input stream
	 */
	public Reader getSource();

	
	/**
	 * Returns a subset of the internal symbol table, with just the symbols that are not ignored by this scanner
	 * @return the symbols that are not ignored by this scanner
	 */
	public TerminalSymbol[] getPublicSymbolTable();

	
	/**
	 * Returns all the symbols recognized by this scanner
	 * @return all the symbols recognized by this scanner
	 */
	public TerminalSymbol[] getInternalSymbolTable();
	
	/**
	 * Returns the scanner project name
	 * @return the scanner project name
	 */
	public String getName();
}
