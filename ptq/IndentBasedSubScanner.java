/*******************************************************************************
 * This work is licensed under the Creative Commons Attribution 4.0 International License. 
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 ******************************************************************************/
package ptq;


import java.io.IOException;


/**
 * Contains the scanning algorythm and represents a single
 * scanner DFA. It also handles symbol actions like DFA start, end and switch, newlines
 * and ignoring symbols. 
 * 
 * 
 * @author Alessio Scalici
 *
 */
class IndentBasedSubScanner extends BaseSubScanner
{
	

	IndentBasedSubScanner(BaseSubScanner o, IndentBasedScanner parent)
	{
		this(o.name, o.initialState);
		this.actions = o.actions;
		this.parent = parent;
	}
	
	void fixActionReferences()
	{
		for (Action a : actions.values())
		{
			if (a.getStart() != null)
				a.setStart(parent.scanners.get(a.getStart().getName()));
			if (a.getGoto() != null)
				a.setGoto(parent.scanners.get(a.getGoto().getName()));
		}
	}
	
	
	/**
	 * Builds a scanner DFA
	 * @param sName the name of DFA in the scanner specification
	 * @param oRootState the DFA initial state
	 */
	IndentBasedSubScanner(String sName, BaseScannerState oRootState)
	{
		super(sName, oRootState);
	}
	
	

	/**
	 * Recognizes the next token. If the recognized token has to be ignored
	 * returns null, otherwise returns the token node. If a lexical error occurs, returns
	 * the special error token.
	 * @return the next token, null if the token has to be ignored, the special error token
	 * if a lexical error occurs
	 * @throws IOException if an I/O error occurs
	 */
	public TerminalNode scan() throws IOException
	{		
		if (parent == null)
			throw new IllegalStateException("parent cannot be null");
		
		// INDENT DELTA
		IndentBasedScanner parent = (IndentBasedScanner)this.parent;
		// INDENT DELTA END
		
		TerminalNode res = null;

		int dataIndex = 0;

		BaseScannerState curState = initialState;

		TerminalSymbol acceptedSymbol = this.parent.getSymbol(SpecialSymbolEnum.ERROR.toInt());
		
		// INDENT DELTA
		boolean newlineFlag = false;
		// INDENT DELTA END

		parent.tokenBegin();
		

		// read the next character
		int prevChar = -1;
		int curChar = parent.getSource().read();

		
		// check if there's no more characters in the stream
		if (curChar < 0)
		{
			// INDENT DELTA
			parent.closePendingBlocks();
			// INDENT DELTA END
			
			res = new TerminalNode(parent.getSymbol(SpecialSymbolEnum.EOS.toInt()));
			res.setData("");
			return res;
		}

		while (curChar >= 0)
		{
			
			buf.add(curChar);
			BaseScannerState next = nextState(curState, curChar);
			if (next == null)
			{
				break; // exit loop
			}
			else
			{
				curState = next;
				if (curState.isFinal())
				{
					// update token data
					parent.newCol(buf.size() - dataIndex);
					dataIndex = buf.size();
					acceptedSymbol = curState.getAcceptedSymbol();
					
				}
			}	

			// unicode newline
			if (curChar == 0xA || curChar == 0xD || curChar == 0xC || curChar == 0xB || curChar == 0x85 || curChar == 0x2028 || curChar == 0x2029)
			{
				if (!(prevChar == 0xD && curChar == 0xA))
					++parent.curLine;
				parent.curCol = BaseScanner.INITIAL_COL;
				
				// indent based
				parent.indenting = true;
				newlineFlag = true;
				
			}
			
			prevChar = curChar;
			curChar = parent.getSource().read();
			
		}// end for
		
		
		// retrieve action for this symbol
		Action act;
		if (actions.containsKey(acceptedSymbol))
			act = actions.get(acceptedSymbol);
		else
			act = null;
		
		
		// INDENT DELTA
		if (parent.indenting)
		{
			if (act != null && act.isIndent())
			{
				parent.level += dataIndex; 
				//System.out.println("Line: "+parent.begLine+" lev += "+data.size());
			}
			else 
			{
				if (!newlineFlag) 
				{
					parent.processLevel();
					
					parent.indenting = false;
				}
				parent.level = 0;
			}
		}
		// INDENT DELTA END
		
		
		// action handling
		if(act != null)
		{ 
			if (act.isPushback() && (act.getGoto() != null || act.getStart() != null || act.isEnd()))
			{
				while (!buf.isEmpty())
					parent.getSource().unread(buf.removeLast());
				parent.tokenReset();
				res = null; // ignore token
			}
			else
				res = new TerminalNode(acceptedSymbol);
			
			if (act.isEnd())
				parent.end();
			if (act.getStart() != null)
				parent.begin(act.getStart());
			if (act.getGoto() != null)
				parent.setCurrentScanner(act.getGoto());	
		}
		else
			res = new TerminalNode(acceptedSymbol);
		
		
		if (res != null)
		{
			if (acceptedSymbol.getIndex() == SpecialSymbolEnum.ERROR.toInt())
			{
				// unread the buffer data
				while(!buf.isEmpty())
					parent.getSource().unread(buf.removeLast());
				parent.tokenReset();
		
				parent.newCol(1);
				res.setData(String.copyValueOf(Character.toChars(parent.getSource().read())));
			}
			else // non errore
			{
				int n = buf.size()-dataIndex;
				for (int i=0; i<n; ++i)
					parent.getSource().unread(buf.removeLast());
				
				res = new TerminalNode(acceptedSymbol);
				
				StringBuffer sbuf = new StringBuffer();
				while (!buf.isEmpty())
					sbuf.append(Character.toChars(buf.removeFirst()));
				res.setData(sbuf.toString());

			}
		}
		
		buf.clear();
		
		if (act != null && act.isIgnore())
			return null; // ignore token
		
		return res;
	}
	
	

	
}
